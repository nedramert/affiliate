<?php
namespace AffiliateBundle\Helpers;

use AffiliateBundle\Factory\CdnInterface;
use Aws\CloudFront\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Blob\Models\CreateContainerOptions;
use WindowsAzure\Blob\Models\PublicAccessType;

class Azure_Helper implements CdnInterface {

    private $azure_account;
    private $azure_key;
    private $azure_protocol;
    private $azure_default_container;

    private $connectionString;
    private $connection;

    public function __construct($azure_account, $azure_key, $azure_protocol, $azure_default_container) {
        $this->azure_account = $azure_account;
        $this->azure_key = $azure_key;
        $this->azure_protocol = $azure_protocol;
        $this->azure_default_container = $azure_default_container;

        $this->setAzureConnection();

    }

    public function upload(UploadedFile $file) {


        try {

            // This part for a new container in azure service. It will be handled after.
            /*
                $createContainerOptions = new CreateContainerOptions();
                $createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);

                $createContainerOptions->addMetaData("key1", "value1");
                $createContainerOptions->addMetaData("key2", "value2");

                $this->connection->createContainer("contents", $createContainerOptions);
            */
            $blob_name = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());


            $fileInstance = fopen($file->getRealPath(), "r");
            $upload = $this->connection->createBlockBlob($this->azure_default_container, $blob_name, $fileInstance);

            if ($upload) {
                $blob = $this->connection->getBlob($this->azure_default_container, $blob_name);

            }

            // This part list all blobs in container
            /*
            $blob_list = $this->connection->listBlobs("contents");
            $blobs = $blob_list->getBlobs();

            foreach($blobs as $blob)
            {
                echo $blob->getName().": ".$blob->getUrl()."<br />";
            }
            */

            $returnData = [
                'fileName' => $blob_name,
                'cdn_url' => "https://".$this->azure_account.".blob.core.windows.net/".$this->azure_default_container."/".$blob_name
            ];

            return $returnData;

        } catch(ServiceException $e) {
            return $e->getMessage();
        }

    }

    private function setConnectionString() {
        $this->connectionString = 'DefaultEndpointsProtocol=' . $this->azure_protocol . ';AccountName='.$this->azure_account.';AccountKey='.$this->azure_key;
    }

    private function getConnectionString() {
        return $this->connectionString;
    }

    private function setAzureConnection() {
        $this->setConnectionString();
        $this->connection = ServicesBuilder::getInstance()->createBlobService($this->getConnectionString());
    }
}