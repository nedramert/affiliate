<?php
namespace AffiliateBundle\Helpers;

use Doctrine\ORM\EntityManager;
use Monolog\Logger;

class Log_Helper {
    private $logger;

    public function __construct(Monolog $logger) {
        $this->logger = $logger;
    }

    public function addLog($logString = "") {
        $log = $this->logger->error('An error occurred');
        return $log;
    }
}