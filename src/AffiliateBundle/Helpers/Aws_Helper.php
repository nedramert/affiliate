<?php
namespace AffiliateBundle\Helpers;

use AffiliateBundle\Factory\CdnInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Aws_Helper implements CdnInterface {


    private $awsclient;
    private $bucket;

    private static $allowedMimeTypes = array('image/jpeg', 'image/png', 'image/gif', 'text/plain');

    public function __construct($awsclient, $bucket)
    {
        $this->awsclient = $awsclient;
        $this->bucket = $bucket;
    }

    public function upload(UploadedFile $file)
    {

        if (!in_array($file->getClientMimeType(), self::$allowedMimeTypes)) {
            throw new \InvalidArgumentException(sprintf('Files of type %s are not allowed.', $file->getClientMimeType()));
        }


        $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());

        $uploadArray = [
            'Bucket'       => $this->bucket,
            'ContentType'  => $file->getClientMimeType(),
            'Key'          => $filename,
            'SourceFile'   => $file->getRealPath(),
            'ACL'          => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY',
            'Metadata'     => array(
                'param1' => 'value 1',
                'param2' => 'value 2'
            )
        ];

        $result = $this->putObject($uploadArray);

        $returnData = [
            'fileName' => $filename,
            'cdn_url' => $result['ObjectURL']
        ];

        return $returnData;
    }

    public function putObject($array) {
        $result = $this->awsclient->putObject($array);
        return $result;
    }

    public function deleteObject($array) {
        $result = $this->awsclient->deleteObject($array);
        return $result;
    }

}