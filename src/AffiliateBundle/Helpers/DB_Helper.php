<?php
namespace AffiliateBundle\Helpers;

use Doctrine\ORM\EntityManager;

class DB_Helper {
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function FindCampaign($id) {
        $campaign = $this->entityManager->getRepository("AffiliateBundle:Campaigns")->getCampaignById($id);
        return $campaign;
    }

    public function fixDomainName($domainName) {
        $newName = trim(str_replace("www","","$domainName"));
        return $newName;
    }
}