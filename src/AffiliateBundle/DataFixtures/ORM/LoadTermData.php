<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace AffiliateBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AffiliateBundle\Entity\Terms;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $term = new Terms();

        $term->setName("Minimum First Deposit");
        $term->setTermType("deposit");
        $term->setIsActive(true);

        $manager->persist($term);
        $manager->flush();

        $term = new Terms();

        $term->setName("Banned Countries");
        $term->setTermType("country");
        $term->setIsActive(true);

        $manager->persist($term);
        $manager->flush();


        $term = new Terms();

        $term->setName("Minimum Lead");
        $term->setTermType("count");
        $term->setIsActive(true);

        $manager->persist($term);
        $manager->flush();

    }
}