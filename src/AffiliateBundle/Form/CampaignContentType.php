<?php

namespace AffiliateBundle\Form;

use AffiliateBundle\Entity\CampaignContent;
use AffiliateBundle\Entity\Content;
use AffiliateBundle\Entity\Repositories\CampaignRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampaignContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('campaignId', 'entity',
            array('class' => 'AffiliateBundle\Entity\Campaigns', 'attr'=>array('style'=>'display:none;'))
        );

        $builder->add('contentId', 'entity',
            array(
                'class' => 'AffiliateBundle\Entity\Content',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->orderBy('c.createdAt', 'DESC');
                }
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AffiliateBundle\Entity\CampaignContent'
        ));
    }

    public function getName()
    {
        return 'affiliate_bundle_campaign_content_type';
    }
}
