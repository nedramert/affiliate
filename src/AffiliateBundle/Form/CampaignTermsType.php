<?php

namespace AffiliateBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class CampaignTermsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('campaignId')
            ->add('commissionId')
            ->add('termId')
            ->add('termValues')
            ->add('insertDate')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AffiliateBundle\Entity\CampaignTerms'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'affiliatebundle_campaignterms';
    }
}
