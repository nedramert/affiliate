<?php
namespace AffiliateBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class ContentType
 * @package AffiliateBundle\Form
 */
class ContentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('objectKey', 'file', array('required' => false, 'label'=>false));
        $builder->add('objectText', 'text', array('required' => false, 'label' => false));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'AffiliateBundle\Entity\Content'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'content';
    }
}