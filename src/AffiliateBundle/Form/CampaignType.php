<?php
namespace AffiliateBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CampaignType
 * @package AffiliateBundle\Form
 */
class CampaignType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('camp_name', null, array('label' => false));
        $builder->add('description', 'textarea', array('label' => false));
        $builder->add('landing_url', null, array('label' => false));
        $builder->add('is_hide', 'checkbox', array('label' => false));


        $builder->add('startDate', 'datetime', array('label' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'));
        $builder->add('endDate', 'datetime', array('label' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'));


        $builder->add('insertUser', 'entity', array(
            'class' => 'AffiliateBundle\Entity\Users',
        ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'AffiliateBundle\Entity\Campaigns'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'campaign';
    }
}