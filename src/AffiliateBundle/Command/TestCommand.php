<?php

namespace AffiliateBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('demo:test')
            ->setDescription('Ekrana bisiler bastiralim')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Ekrana hangi adı bastırıcaksın ?'
            )
            ->addOption(
                'buyuk',
                null,
                InputOption::VALUE_NONE,
                'Eger set edilirse tüm yazı büyük olacak.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        if ($name) {
            $text = 'Merhaba '.$name;
        } else {
            $text = 'Merhaba';
        }

        if ($input->getOption('buyuk')) {
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }
}