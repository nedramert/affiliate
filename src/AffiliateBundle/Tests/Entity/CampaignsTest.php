<?php

use AffiliateBundle\Entity\Campaigns;

class CampaignsTest extends \PHPUnit_Framework_TestCase
{
    public function testCampName()
    {
        $campaign = new Campaigns();

        $this->assertEquals('Hello World', $campaign->setCampName('Hello World'));
    }
}