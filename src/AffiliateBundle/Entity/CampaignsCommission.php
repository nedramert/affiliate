<?php

namespace AffiliateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignsCommission
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AffiliateBundle\Entity\Repositories\CampCommRepository")
 */
class CampaignsCommission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Campaigns", inversedBy="caco")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaignId;

    /**
     * @ORM\ManyToOne(targetEntity="Commission", inversedBy="caco")
     * @ORM\JoinColumn(name="commission_id", referencedColumnName="id")
     */
    private $commissionId;

    /**
     * @var string
     *
     * @ORM\Column(name="pay", type="string", length=255,nullable=true)
     */
    private $pay = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=255, nullable=true)
     */
    private $paymentType = null;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaignId
     *
     * @param integer $campaignId
     *
     * @return CampaignsCommission
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * Get campaignId
     *
     * @return integer
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * Set commissionId
     *
     * @param integer $commissionId
     *
     * @return CampaignsCommission
     */
    public function setCommissionId($commissionId)
    {
        $this->commissionId = $commissionId;

        return $this;
    }

    /**
     * Get commissionId
     *
     * @return integer
     */
    public function getCommissionId()
    {
        return $this->commissionId;
    }

    /**
     * Set pay
     *
     * @param string $pay
     *
     * @return CampaignsCommission
     */
    public function setPay($pay)
    {
        $this->pay = $pay;

        return $this;
    }

    /**
     * Get pay
     *
     * @return string
     */
    public function getPay()
    {
        return $this->pay;
    }

    /**
     * Set paymentType
     *
     * @param string $paymentType
     *
     * @return CampaignsCommission
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }
}
