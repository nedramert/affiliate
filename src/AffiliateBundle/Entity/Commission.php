<?php

namespace AffiliateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Commission
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Commission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="com_name", type="string", length=255)
     */
    private $com_name;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\OneToMany(targetEntity="CampaignsCommission" , mappedBy="commission" , cascade={"all"})
     * */
    protected $caco;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());

        $this->caco = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedValue()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="approved", type="boolean")
     */
    private $approved;

    /**
     * @var string
     *
     * @ORM\Column(name="com_note", type="text")
     */
    private $comNote;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Commission
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return Commission
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Commission
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Commission
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }



    /**
     * Set comName
     *
     * @param string $comName
     *
     * @return Commission
     */
    public function setComName($comName)
    {
        $this->com_name = $comName;

        return $this;
    }

    /**
     * Get comName
     *
     * @return string
     */
    public function getComName()
    {
        return $this->com_name;
    }

    public function __toString() {
        return $this->com_name;
    }

    /**
     * Set comNote
     *
     * @param string $comNote
     *
     * @return Commission
     */
    public function setComNote($comNote)
    {
        $this->comNote = $comNote;

        return $this;
    }

    /**
     * Get comNote
     *
     * @return string
     */
    public function getComNote()
    {
        return $this->comNote;
    }

    /**
     * Add caco
     *
     * @param \AffiliateBundle\Entity\CampaignsCommission $caco
     *
     * @return Commission
     */
    public function addCaco(\AffiliateBundle\Entity\CampaignsCommission $caco)
    {
        $this->caco[] = $caco;

        return $this;
    }

    /**
     * Remove caco
     *
     * @param \AffiliateBundle\Entity\CampaignsCommission $caco
     */
    public function removeCaco(\AffiliateBundle\Entity\CampaignsCommission $caco)
    {
        $this->caco->removeElement($caco);
    }

    /**
     * Get caco
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCaco()
    {
        return $this->caco;
    }

}
