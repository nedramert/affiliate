<?php

namespace AffiliateBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;

class CampaignContentRepository extends EntityRepository
{
    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getContentTypesByCampId($id)
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 10;

        $qb = $this->createQueryBuilder('c');

        $qb->where('c.campaignId = :id')
            ->innerJoin('AffiliateBundle\Entity\Content','con','WITH','c.contentId = con.id')
            ->groupBy('con.objectType')
            ->select('con, c.id AS campContId, COUNT(con.id) as contCount');

        return $qb->setParameters(array('id'=>$id))
                  ->getQuery()
                  ->setResultCacheDriver($predis)
                  ->setResultCacheLifetime($expire)
                  ->getScalarResult();
    }

    public function getContensByTypeAndCampId($camp_id, $type)
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());
        $expire = 50;

        $qb = $this->createQueryBuilder('c')->select('c');

        $qb->where('c.campaignId = :camp_id')
           ->innerJoin('AffiliateBundle\Entity\Content','con','WITH','c.contentId = con.id')
           ->andWhere('con.objectType = :cont_type')
           ->select('con');

        return $qb->setParameters(array('camp_id' => $camp_id, 'cont_type' => $type))
                  ->getQuery()
                  ->setResultCacheDriver($predis)
                  ->setResultCacheLifetime($expire)
                  ->getScalarResult() ?: null;
    }
}
