<?php
namespace AffiliateBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;

class UsersRepository extends EntityRepository
{
    public function totalUsers() {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 20;

        $qb = $this->createQueryBuilder('u')->select('count(u.id)');

        $qb->where('u.roles LIKE :roles')->setParameter('roles', '%ROLE_USER%');

        return $qb->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getSingleScalarResult();
    }

    public function usersByDay() {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 20;

        $qb = $this->createQueryBuilder('u')->select('count(u.id) as counted, DATE(u.created) as registerDate');

        $qb->where('u.roles LIKE :roles')->setParameter('roles', '%ROLE_USER%');

        $qb->groupBy('registerDate')->orderBy('DATE(u.created)', 'ASC');

        return $qb->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getResult();
    }
}
