<?php

namespace AffiliateBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;

class ContentRepository extends EntityRepository
{
    public function totalContent() {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 20;

        $qb = $this->createQueryBuilder('c')->select('count(c.id)');


        return $qb->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getSingleScalarResult();
    }
}
