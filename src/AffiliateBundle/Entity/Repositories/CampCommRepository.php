<?php

namespace AffiliateBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;

class CampCommRepository extends EntityRepository
{
    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */

    public function deleteModelsByCampaignId($campaignId) {
        $qb = $this->createQueryBuilder("c");
        $qb->delete('AffiliateBundle:CampaignsCommission','c');
        $qb->andWhere($qb->expr()->eq('c.campaignId', ':id'));
        $qb->setParameter(':id', $campaignId);

        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getModelsByCampaignId($campaignId) {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 10;

        $queryBuilder = $this->createQueryBuilder("c")->select("co");
        $queryBuilder->innerJoin('AffiliateBundle:Commission', 'co', 'WITH', 'co.id = c.commissionId');
        $queryBuilder->where("c.campaignId = :id");


        $result = $queryBuilder->setParameters(array('id'=>$campaignId))
            ->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getResult();

        return $result;
    }

    public function getCampaignById($id)
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 5;

        $qb = $this->createQueryBuilder('c')->select('c');

        $qb->where('c.id = :id');

        return $qb->setParameters(array('id'=>$id))
            ->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getOneOrNullResult();

    }
}
