<?php

namespace AffiliateBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;

class CampaignRepository extends EntityRepository
{
    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */

    public function getCampaignById($id)
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 5;

        $qb = $this->createQueryBuilder('c')->select('c');

        $qb->where('c.id = :id');

        return $qb->setParameters(array('id'=>$id))
            ->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getOneOrNullResult();
    }

    public function totalCampaign() {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 20;

        $qb = $this->createQueryBuilder('c')->select('count(c.id)');


        return $qb->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getSingleScalarResult();
    }

    public function getActiveCampaigns()
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 10;

        $qb = $this->createQueryBuilder('c')->select('c');
        $todayDateTime = new \DateTime();
        $qb->where($qb->expr()->orX(
                $qb->expr()->gte('c.endDate', ':end_date'),
                $qb->expr()->isNull('c.endDate')
            ))
            ->setParameter(':end_date', $todayDateTime);

        return $qb->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getResult();
    }
}
