<?php

namespace AffiliateBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignTerms
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class CampaignTerms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Campaigns", inversedBy="terms")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaignId;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Commission", inversedBy="terms")
     * @ORM\JoinColumn(name="commission_id", referencedColumnName="id")
     */
    private $commissionId;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Terms", inversedBy="camp_terms")
     * @ORM\JoinColumn(name="term_id", referencedColumnName="id")
     */
    private $termId;

    /**
     * @var string
     *
     * @ORM\Column(name="term_values", type="text")
     */
    private $termValues;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_date", type="datetime")
     */
    private $insertDate;


    /**
     * Construct
     */
    public function __construct()
    {
        $this->setInsertDate(new \DateTime());

        $this->caco = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaignId
     *
     * @param integer $campaignId
     *
     * @return CampaignTerms
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * Get campaignId
     *
     * @return integer
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * Set commissionId
     *
     * @param integer $commissionId
     *
     * @return CampaignTerms
     */
    public function setCommissionId($commissionId)
    {
        $this->commissionId = $commissionId;

        return $this;
    }

    /**
     * Get commissionId
     *
     * @return integer
     */
    public function getCommissionId()
    {
        return $this->commissionId;
    }

    /**
     * Set termId
     *
     * @param integer $termId
     *
     * @return CampaignTerms
     */
    public function setTermId($termId)
    {
        $this->termId = $termId;

        return $this;
    }

    /**
     * Get termId
     *
     * @return integer
     */
    public function getTermId()
    {
        return $this->termId;
    }

    /**
     * Set termValues
     *
     * @param string $termValues
     *
     * @return CampaignTerms
     */
    public function setTermValues($termValues)
    {
        $this->termValues = $termValues;

        return $this;
    }

    /**
     * Get termValues
     *
     * @return string
     */
    public function getTermValues($getStyle = null)
    {
        if ($getStyle != null) {
            $parse = json_decode($this->termValues);


            $listString = "";
            $values = $parse->values;
            foreach ($values as $value) {
                if (empty($listString))
                    $listString.=$value;
                else
                    $listString.=  ", " . $value;
            }

            return $listString;

        }
        return $this->termValues;
    }

    /**
     * Set insertDate
     *
     * @param \DateTime $insertDate
     *
     * @return CampaignTerms
     */
    public function setInsertDate($insertDate)
    {
        $this->insertDate = $insertDate;

        return $this;
    }

    /**
     * Get insertDate
     *
     * @return \DateTime
     */
    public function getInsertDate()
    {
        return $this->insertDate;
    }
}
