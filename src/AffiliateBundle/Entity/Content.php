<?php

namespace AffiliateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Content
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AffiliateBundle\Entity\Repositories\ContentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Content
{

    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';
    const TYPE_TEXT = 'text';

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="object_key", type="string", length=255)
     */
    private $objectKey;

    /**
     * @var string
     *
     * @ORM\Column(name="object_type", type="string", columnDefinition="ENUM('visible', 'invisible')")
     */
    private $objectType;

    /**
     * @var string
     *
     * @ORM\Column(name="object_url", type="text")
     */
    private $objectUrl = "";

    /**
     * @var string
     *
     * @ORM\Column(name="object_size", type="string", length=255)
     */
    private $objectSize = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="objectText", type="string")
     */
    private $objectText;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="\UsersBundle\Entity\UserCampaign", mappedBy="user_campaigns")
     */
    private $campaigns;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set objectKey
     *
     * @param string $objectKey
     *
     * @return Content
     */
    public function setObjectKey($objectKey)
    {
        $this->objectKey = $objectKey;

        return $this;
    }

    /**
     * Get objectKey
     *
     * @return string
     */
    public function getObjectKey()
    {
        return $this->objectKey;
    }

    /**
     * Set objectType
     *
     * @param string $objectType
     *
     * @return Content
     */
    public function setObjectType($objectType)
    {
        $enumTypes = array(self::TYPE_IMAGE, self::TYPE_VIDEO, self::TYPE_TEXT );

        if(!in_array($objectType, $enumTypes)){
            throw new \InvalidArgumentException($objectType . " türü kabul ettiğimiz içerik türleri arasında değildir!");
        }
        $this->objectType = $objectType;

        return $this;
    }

    static function getTypeConstants($const = false) {
        $contentReflection = new \ReflectionClass(__CLASS__);

        if($const !== false){

            return $contentReflection->getConstant($const);

        }else{

            $constants = $contentReflection->getConstants();
            $typeConstants = array();

            foreach ($constants as $key =>  $value) {

                if(substr($key,0,4) == "TYPE"){
                    $typeConstants[$key] = $value;
                }
            }

            return $typeConstants;
        }
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set objectUrl
     *
     * @param string $objectUrl
     *
     * @return Content
     */
    public function setObjectUrl($objectUrl)
    {
        $this->objectUrl = $objectUrl;

        return $this;
    }

    /**
     * Get objectUrl
     *
     * @return string
     */
    public function getObjectUrl()
    {
        return $this->objectUrl;
    }

    /**
     * Set objectSize
     *
     * @param string $objectSize
     *
     * @return Content
     */
    public function setObjectSize($objectSize)
    {
        $this->objectSize = $objectSize;

        return $this;
    }

    /**
     * Get objectSize
     *
     * @return string
     */
    public function getObjectSize()
    {
        return $this->objectSize;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Content
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function __toString() {
        return $this->objectKey;
    }

    /**
     * Set objectText
     *
     * @param string $objectText
     *
     * @return Content
     */
    public function setObjectText($objectText)
    {
        $this->objectText = $objectText;

        return $this;
    }

    /**
     * Get objectText
     *
     * @return string
     */
    public function getObjectText()
    {
        return $this->objectText;
    }
}
