<?php

namespace AffiliateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Terms
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AffiliateBundle\Entity\Repositories\TermsRepository")
 */
class Terms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="term_type", type="string", length=255)
     */
    private $termType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Terms
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set termType
     *
     * @param string $termType
     *
     * @return Terms
     */
    public function setTermType($termType)
    {
        $this->termType = $termType;

        return $this;
    }

    /**
     * Get termType
     *
     * @return string
     */
    public function getTermType()
    {
        return $this->termType;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Terms
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function __toString() {
        return $this->name;
    }
}
