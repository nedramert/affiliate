<?php

namespace AffiliateBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Campaigns
 *
 * @ORM\Table("campaign")
 * @ORM\Entity(repositoryClass="AffiliateBundle\Entity\Repositories\CampaignRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Campaigns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;


    /**
     * Construct
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());

        $this->caco = new ArrayCollection();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedValue()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = false;

    /**
     * @var string
     *
     * @ORM\Column(name="camp_name", type="string", length=255)
     */
    private $camp_name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="landing_url", type="string", length=255)
     */
    private $landingUrl;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_hide", type="boolean")
     */
    private $isHide = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_delete", type="boolean")
     */
    private $isDelete = false;

    /**
     * @var integer
     * @ORM\Column(name="update_user_id", type="integer", nullable=true)
     */
    protected $updateUser;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="campaigns")
     * @ORM\JoinColumn(name="insert_user", referencedColumnName="id")
     */
    protected $insertUser;

    /**
     * @ORM\OneToMany(targetEntity="CampaignsCommission" , mappedBy="commissionId" , cascade={"all"})
     *
     */
    protected $caco;

    /**
     * @ORM\OneToMany(targetEntity="CampaignTerms" , mappedBy="campaignId" , cascade={"all"})
     *
     */
    protected $campaignTerms;

    /**
     * @ORM\OneToMany(targetEntity="UsersBundle\Entity\UserCampaign" , mappedBy="campaign")
     */
    protected $users;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $endDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return \DateTime
     */
    public function getDeletedTime()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedTime($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Campaigns
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Campaigns
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set landingUrl
     *
     * @param string $landingUrl
     *
     * @return Campaigns
     */
    public function setLandingUrl($landingUrl)
    {
        $this->landingUrl = $landingUrl;

        return $this;
    }

    /**
     * Get landingUrl
     *
     * @return string
     */
    public function getLandingUrl()
    {
        return $this->landingUrl;
    }

    /**
     * Set isHide
     *
     * @param boolean $isHide
     *
     * @return Campaigns
     */
    public function setIsHide($isHide)
    {
        $this->isHide = $isHide;

        return $this;
    }

    /**
     * Get isHide
     *
     * @return boolean
     */
    public function getIsHide()
    {
        return $this->isHide;
    }

    /**
     * Set isDelete
     *
     * @param boolean $isDelete
     *
     * @return Campaigns
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * Get isDelete
     *
     * @return boolean
     */
    public function getIsDelete()
    {
        return $this->isDelete;
    }

    /**
     * Set updateUserId
     *
     * @param integer $updateUserId
     * @return Campaigns
     */
    public function setUpdateUser($updateUser)
    {

        $this->updateUser = $updateUser;

        return $this;
    }

    /**
     * Get updateUserId
     *
     * @return integer
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }

    /**
     * Set insertUserId
     *
     * @param integer $insertUserId
     *
     * @return Campaigns
     */
    public function setInsertUserId($insertUserId)
    {
        $this->insertUserId = $insertUserId;

        return $this;
    }

    /**
     * Get insertUserId
     *
     * @return integer
     */
    public function getInsertUserId()
    {
        return $this->insertUserId;
    }

    /**
     * Set campName
     *
     * @param string $campName
     *
     * @return Campaigns
     */
    public function setCampName($campName)
    {
        $this->camp_name = $campName;

        return $this;
    }

    /**
     * Get campName
     *
     * @return string
     */
    public function getCampName()
    {
        return $this->camp_name;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Campaigns
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Campaigns
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set insertUser
     *
     * @param \AffiliateBundle\Entity\Users $insertUser
     *
     * @return Campaigns
     */
    public function setInsertUser(\AffiliateBundle\Entity\Users $insertUser = null)
    {
        $this->insertUser = $insertUser;

        return $this;
    }

    /**
     * Get insertUser
     *
     * @return \AffiliateBundle\Entity\Users
     */
    public function getInsertUser()
    {
        return $this->insertUser;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('camp_name', new Length(array(
            'min' => '5',
            'max' => '50',
        )));
        $metadata->addPropertyConstraint('landingUrl', new NotBlank());
    }

    public function __toString() {
        return $this->camp_name;
    }



    /**
     * Add caco
     *
     * @param \AffiliateBundle\Entity\CampaignsCommission $caco
     *
     * @return Campaigns
     */
    public function addCaco(\AffiliateBundle\Entity\CampaignsCommission $caco)
    {
        $this->caco[] = $caco;

        return $this;
    }

    /**
     * Remove caco
     *
     * @param \AffiliateBundle\Entity\CampaignsCommission $caco
     */
    public function removeCaco(\AffiliateBundle\Entity\CampaignsCommission $caco)
    {
        $this->caco->removeElement($caco);
    }

    /**
     * Get caco
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCaco()
    {
        return $this->caco;
    }

    /**
     * Add campaignTerm
     *
     * @param \AffiliateBundle\Entity\CampaignTerms $campaignTerm
     *
     * @return Campaigns
     */
    public function addCampaignTerm(\AffiliateBundle\Entity\CampaignTerms $campaignTerm)
    {
        $this->campaignTerms[] = $campaignTerm;

        return $this;
    }

    /**
     * Remove campaignTerm
     *
     * @param \AffiliateBundle\Entity\CampaignTerms $campaignTerm
     */
    public function removeCampaignTerm(\AffiliateBundle\Entity\CampaignTerms $campaignTerm)
    {
        $this->campaignTerms->removeElement($campaignTerm);
    }

    /**
     * Get campaignTerms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignTerms()
    {
        return $this->campaignTerms;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Campaigns
     */
    public function setStartDate($startDate)
    {

        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Campaigns
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

}
