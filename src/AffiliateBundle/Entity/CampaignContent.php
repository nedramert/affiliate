<?php

namespace AffiliateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignContent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AffiliateBundle\Entity\Repositories\CampaignContentRepository")
 */
class CampaignContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Campaigns", inversedBy="camp_content_campaignid")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaignId;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Content", inversedBy="camp_content_contentid")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     */
    private $contentId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaignId
     *
     * @param integer $campaignId
     *
     * @return CampaignContent
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    /**
     * Get campaignId
     *
     * @return integer
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * Set contentId
     *
     * @param integer $contentId
     *
     * @return CampaignContent
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;

        return $this;
    }

    /**
     * Get contentId
     *
     * @return integer
     */
    public function getContentId()
    {
        return $this->contentId;
    }
}

