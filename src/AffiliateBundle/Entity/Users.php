<?php
namespace AffiliateBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\Util\SecureRandom;
/**
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AffiliateBundle\Entity\Repositories\UsersRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Users extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Campaigns", mappedBy="insertUser")
     */
    protected $campaigns;

    /**
     * @var String
     * @ORM\Column(name="fullname", type="string", length=255)
     */
    protected $fullName;

    /**
     * @var String
     * @ORM\Column(name="tc", type="string", length=255, nullable=true)
     */
    protected $tc;

    /**
     * @var String
     * @ORM\Column(name="tax_no", type="string", length=255, nullable=true)
     */
    protected $taxNo;

    /**
     * @var String
     * @ORM\Column(name="tax_admin", type="string", length=255, nullable=true)
     */
    protected $taxAdmin;

    /**
     * @var String
     * @ORM\Column(name="phone", type="string", length=255)
     */
    protected $phone;

    /**
     * @var String
     * @ORM\Column(name="address", type="text")
     */
    protected $address;

    /**
     * @var String
     * @ORM\Column(name="country", type="string", length=255)
     */
    protected $country;

    /**
     * @var String
     * @ORM\Column(name="city", type="string", length=255)
     */
    protected $city;

    /**
     * @ORM\OneToMany(targetEntity="\UsersBundle\Entity\UserDomains" , mappedBy="user" , cascade={"all"})
     */
    protected $domains;

    /**
     * @ORM\OneToMany(targetEntity="\UsersBundle\Entity\UserCampaign" , mappedBy="user")
     */
    protected $camps;

    /**
     * Add campaign
     *
     * @param \AffiliateBundle\Entity\Campaigns $campaign
     *
     * @return User
     */
    public function addCampaign(\AffiliateBundle\Entity\Campaigns $campaign)
    {
        $this->campaigns[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \AffiliateBundle\Entity\Campaigns $campaign
     */
    public function removeCampaign(\AffiliateBundle\Entity\Campaigns $campaign)
    {
        $this->campaigns->removeElement($campaign);
    }

    /**
     * Get campaigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    public function __construct()
    {
        parent::__construct();


        $this->setCreated(new \DateTime());

        $this->campaigns = new ArrayCollection();
        $this->domains = new ArrayCollection();

        $this->roles = array('ROLE_USER');

    }

    /**
     * @ORM\PrePersist
     */
    public function generateUserName() {
        $generator = new SecureRandom();
        $random = $generator->nextBytes(6);

        $newName = trim(preg_replace('!\s+!', '-', $this->getFullName()));

        $newUserName = $newName."-".substr(md5($random),0,10);
        $this->username = $newUserName;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Users
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set tc
     *
     * @param string $tc
     *
     * @return Users
     */
    public function setTc($tc)
    {
        $this->tc = $tc;

        return $this;
    }

    /**
     * Get tc
     *
     * @return string
     */
    public function getTc()
    {
        return $this->tc;
    }

    /**
     * Set taxNo
     *
     * @param string $taxNo
     *
     * @return Users
     */
    public function setTaxNo($taxNo)
    {
        $this->taxNo = $taxNo;

        return $this;
    }

    /**
     * Get taxNo
     *
     * @return string
     */
    public function getTaxNo()
    {
        return $this->taxNo;
    }

    /**
     * Set taxAdmin
     *
     * @param string $taxAdmin
     *
     * @return Users
     */
    public function setTaxAdmin($taxAdmin)
    {
        $this->taxAdmin = $taxAdmin;

        return $this;
    }

    /**
     * Get taxAdmin
     *
     * @return string
     */
    public function getTaxAdmin()
    {
        return $this->taxAdmin;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Users
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Users
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Users
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Users
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }



    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Users
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }



    /**
     * Add domain
     *
     * @param \UsersBundle\Entity\UserDomains $domain
     *
     * @return Users
     */
    public function addDomain(\UsersBundle\Entity\UserDomains $domain)
    {
        $this->domains[] = $domain;

        return $this;
    }

    /**
     * Remove domain
     *
     * @param \UsersBundle\Entity\UserDomains $domain
     */
    public function removeDomain(\UsersBundle\Entity\UserDomains $domain)
    {
        $this->domains->removeElement($domain);
    }

    /**
     * Get domains
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDomains()
    {
        return $this->domains;
    }
}
