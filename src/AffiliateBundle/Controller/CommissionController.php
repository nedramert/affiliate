<?php

namespace AffiliateBundle\Controller;

use Proxies\__CG__\AffiliateBundle\Entity\Commission;
use AffiliateBundle\Form\CommissionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AffiliateBundle\Form\ContentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AffiliateBundle\Entity\Campaigns;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AffiliateBundle\Entity\CampaignTerms;
use AffiliateBundle\Form\CampaignTermsType;

use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CommissionController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/_get_term_values", name="ajax_get_term_values")
     * @Method({"POST"})
     */
    public function getTermValues(Request $request) {

        try {
            $termId = $request->get('id');
            $em = $this->getDoctrine()->getManager();

            $term = $em->getRepository("AffiliateBundle:Terms")->find($termId);

            $render = "";
            $type = "";
            switch ($term->getTermType()) {
                case "count":
                    $render = $this->renderView("@Affiliate/Commission/templates/count.html.twig", array('termId' => $termId));
                    break;
                case "deposit":
                    $render = $this->renderView("@Affiliate/Commission/templates/deposit.html.twig", array('termId' => $termId));
                    break;
                case "country":
                    $render = $this->renderView("@Affiliate/Commission/templates/country.html.twig", array('termId' => $termId));
                    break;

                default:

                    break;
            }

            $type = $term->getTermType();

            return new JsonResponse(array('valueDom' => $render, 'type'=>$type));
        } catch (BadRequestHttpException $e) {
            $this->get("affiliate.helper.log")->addLog($e->getMessage());
            return new JsonResponse(array('badRequest' => $e->getMessage()));
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/commissions", name="commission_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commissions = $em->getRepository("AffiliateBundle:Commission")->findAll();

        $data = array();
        $data['commissions'] = $commissions;
        return $this->render('AffiliateBundle:Commission:list.html.twig', $data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/commissions/add", name="commission_add")
     */
    public function addAction() {

        $em = $this->getDoctrine()->getManager();
        $commission = new Commission();

        $form = $this->createForm(new CommissionType(), $commission);


        return $this->render('AffiliateBundle:Commission:add.html.twig', array('form' => $form->createView()));
    }
}
