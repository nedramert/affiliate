<?php

namespace AffiliateBundle\Controller;

use AffiliateBundle\Entity\CampaignContent;
use AffiliateBundle\Entity\CampaignTerms;
use AffiliateBundle\Entity\Terms;
use AffiliateBundle\Form\CampaignContentType;
use AffiliateBundle\Form\CampaignTermsType;
use AffiliateBundle\Form\ContentType;
use AffiliateBundle\Entity\Commission;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use AffiliateBundle\Entity\Campaigns;
use AffiliateBundle\Entity\Users;
use AffiliateBundle\Entity\Content;
use AffiliateBundle\Form\CampaignType;
use AffiliateBundle\Entity\CampaignsCommission;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class CampaignController
 * @package AffiliateBundle\Controller
 */
class CampaignController extends Controller
{


    /**
     * @Route("/campaigns/{campaign}/unhide", name="campaign_unhide")
     */
    public function unhideAction(Campaigns $campaign) {
        if ($campaign) {
            $em = $this->getDoctrine()->getManager();
            $tr = $this->get('translator');

            $campaign->setIsHide(false);
            $em->merge($campaign);
            $em->flush();

            $this->get('session')->getFlashBag()->add('campaigncontent_notice', $tr->trans('campaign.hide_action', array(), 'AppBundle'));
            return $this->redirectToRoute("campaign_list");
        }
    }

    /**
     * @Route("/campaigns/{campaign}/hide", name="campaign_hide")
     */
    public function hideAction(Campaigns $campaign) {
        if ($campaign) {
            $em = $this->getDoctrine()->getManager();
            $tr = $this->get('translator');

            $campaign->setIsHide(true);
            $em->merge($campaign);
            $em->flush();

            $this->get('session')->getFlashBag()->add('campaigncontent_notice', $tr->trans('campaign.hide_action', array(), 'AppBundle'));
            return $this->redirectToRoute("campaign_list");
        }
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/campaigns/unrelate-content/{campaignContent}", name="campaign_content_unrelate")
     * @Method({"GET"})
     */

    public function unrelateContentAction(CampaignContent $campaignContent) {
        $em = $this->getDoctrine()->getManager();
        $tr = $this->get('translator');

        if ($campaignContent) {

            $campaign = $campaignContent->getCampaignId()->getId();

            $em->remove($campaignContent);
            $em->flush();

            $this->get('session')->getFlashBag()->add('campaigncontent_notice', $tr->trans('campaign_content.related', array(), 'AffiliateBundle'));
            return $this->redirectToRoute("campaign_edit", array('id' => $campaign));
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/campaigns/relate-content", name="campaign_content_relate")
     */
    public function relateContentAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $tr = $this->get('translator');

        $campaignContent = new CampaignContent();
        $form = $this->createForm(new CampaignContentType(), $campaignContent);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $data = $form->getData();
                $campaignId = $data->getCampaignId()->getId();

                $em->persist($campaignContent);
                $em->flush();

                $this->get('session')->getFlashBag()->add('campaigncontent_notice', $tr->trans('campaign_content.related', array(), 'AffiliateBundle'));
                return $this->redirectToRoute("campaign_edit", array('id' => $campaignId));
            }

        }

    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/campaigns/{campaign_id}/manage", name="campaign_commission_manager")
     */
    public function manageCampaignAction($campaign_id) {
        $em = $this->getDoctrine()->getManager();

        $campaign = $em->getRepository("AffiliateBundle:Campaigns")->find($campaign_id);
        $campaign_commissions = $em->getRepository("AffiliateBundle:CampaignsCommission")->findBy(array('campaignId' => $campaign_id));
        $commissions = $em->getRepository("AffiliateBundle:Commission")->findAll();
        $terms = $em->getRepository("AffiliateBundle:Terms")->findAll();
        $term_ops = $em->getRepository("AffiliateBundle:TermsOps")->findAll();

        $campaign_commissions_terms = $em->getRepository("AffiliateBundle:CampaignTerms")->findBy(array('campaignId' => $campaign_id));

        $comList = [];
        foreach ($commissions as $comm) {
            $comList[] = $comm->getComName();
        }

        $returnData = [
            'campaign' => $campaign,
            'campaign_commissions' => $campaign_commissions,
            'commissions' => $commissions,
            'comList' => $comList,
            'terms' => $terms,
            'term_ops' => $term_ops,
            'campaign_terms' => $campaign_commissions_terms
        ];

        return $this->render("AffiliateBundle:Commission:manage.html.twig", $returnData);
    }


    /**
     * @param Campaigns $campaign
     * @Route ("/campaigns/{campaign}/delete", name="campaign_delete")
     * @return Mixed
     */
    public function deleteCampaignAction(Campaigns $campaign) {
        if (!$campaign) {
            throw $this->createNotFoundException('No campaign found');
        }

        $tr = $this->get('translator');

        $em = $this->getDoctrine()->getManager();

        $em->remove($campaign);
        $em->flush();

        $this->get('session')->getFlashBag()->add('campaign_notice', $tr->trans('campaign.deleted', array(), 'AffiliateBundle'));

        return $this->redirect($this->generateUrl('campaign_list'));
    }


    /**
     * @param Request $request
     * @param integer $id
     * @Route ("/campaigns/{id}/edit", name="campaign_edit")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $tr = $this->get('translator');

        try {
            $campaign = $em->getRepository("AffiliateBundle:Campaigns")->find($id);
            $content = new Content();
            $campCommissions = $em->getRepository("AffiliateBundle:CampaignsCommission")->getModelsByCampaignId($id);
            $allCommissions = $em->getRepository("AffiliateBundle:Commission")->findAll();
            $relatedContents = $em->getRepository("AffiliateBundle:CampaignContent")->findBy(array('campaignId' => $id));

        } catch (\Doctrine\ORM\ORMException $e) {
            $this->get("affiliate.helper.log")->addLog($e->getMessage());
            $this->get("session")->getFlashBag()->add('campaign_notice', $e->getMessage());
            return $this->redirectToRoute('campaign_list');
        }

        $form = $this->createForm(new CampaignType(), $campaign);

        $campcontent = new CampaignContent();
        $campcontent->setCampaignId($campaign);
        $content_form = $this->createForm(new CampaignContentType(), $campcontent);


        $request = $this->get('request_stack')->getCurrentRequest();
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->getMethod() == 'POST') {
            try {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $campaign->setUpdateUser($currentUser->getId());

                    $em->merge($campaign);
                    $em->flush();

                    $comms = $request->get("commissions");

                    $em->getRepository("AffiliateBundle:CampaignsCommission")->deleteModelsByCampaignId($id);

                    foreach($comms as $com) {
                        $commission_model = $em->getRepository("AffiliateBundle:Commission")->find($com);
                        $camcom = new CampaignsCommission();
                        $camcom->setCommissionId($commission_model);
                        $camcom->setCampaignId($campaign);
                        $em->persist($camcom);
                        $em->flush();
                    }


                    $this->get('session')->getFlashBag()->add('campaign_notice', $tr->trans('campaign.edited', array(), 'AffiliateBundle'));

                    return $this->redirect($this->generateUrl('campaign_edit', array('id' => $id)));
                }
            } catch (HttpException $e) {
                $this->get("session")->getFlashBag()->add('campaign_notice', $e->getMessage());
                return $this->redirectToRoute('campaign_list');
            } catch (Exception $e) {
                $this->get("session")->getFlashBag()->add('campaign_notice', $e->getMessage());
                return $this->redirectToRoute('campaign_list');
            }
        }


        $data = array(
            'id' => $id,
            'form' => $form->createView(),
            'campaign' => $campaign,
            'campCommission' => $campCommissions,
            'allCommission' => $allCommissions,
            'content_form' => $content_form->createView(),
            'relatedContents' => $relatedContents
        );

        return $this->render('AffiliateBundle:Campaign:edit.html.twig', $data);
    }

    /**
     * @param string $name
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/campaigns/add" , name="campaign_add")
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $campaign = new Campaigns();
        $form = $this->createForm(new CampaignType(), $campaign);

        $commissions = $em->getRepository("AffiliateBundle:Commission")->findAll();

        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $campaign->setInsertUser($currentUser);

                $em->persist($campaign);
                $em->flush();
                $lastCampaign = $campaign->getId();

                $comms = $request->get("commissions");

                foreach($comms as $com) {
                    $commission_model = $em->getRepository("AffiliateBundle:Commission")->find($com);
                    $camcom = new CampaignsCommission();
                    $camcom->setCommissionId($commission_model);
                    $camcom->setCampaignId($campaign);
                    $em->persist($camcom);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('campaign_notice', 'Campaign added.');

                //return $this->redirect($this->generateUrl('campaign_edit', array('id' => $lastCampaign)));
                return $this->redirect($this->generateUrl('campaign_commission_manager', array('campaign_id' => $lastCampaign)));
            }
        }


        return $this->render('AffiliateBundle:Campaign:add.html.twig', array('commission' => $commissions, 'form' => $form->createView()));
    }

    /**
     * @Route ("/campaigns", name="campaign_list")
     * @return view
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $allCampaigns = $em->getRepository("AffiliateBundle:Campaigns")->findAll();


        $find = $this->get("affiliate.helper.db")->FindCampaign(6);


        return $this->render('AffiliateBundle:Campaign:list.html.twig', array("campaigns" => $allCampaigns, "find" => $find));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/campaigns/{campaign}/terms/{term}/delete", name="campaigns_terms_delete")
     */

    public function campaignsTermsDeleteAction(Campaigns $campaign, CampaignTerms $term, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $delete = $em->remove($term);
        $em->flush();


        $this->get('session')->getFlashBag()->add('terms_notice', 'Campaign\'s term deleted.');

        return $this->redirectToRoute("campaign_commission_manager", array('campaign_id' => $campaign->getId()));
    }

}
