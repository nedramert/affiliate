<?php

namespace AffiliateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AffiliateBundle\Entity\Terms;
use AffiliateBundle\Entity\Campaigns;
use AffiliateBundle\Entity\CampaignTerms;

class TermsController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/campaigns/{campaign}/terms", name="campaign_add_term")
     */
    public function termsAddAction(Campaigns $campaign, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $terms = $request->get('condition');
        $termValues = $request->get('condition-value');
        $termops = $request->get('term_op');
        $commissionObj = $em->getRepository("AffiliateBundle:Commission")->find($request->get('commission_model_id'));

        unset($terms[0]);
        unset($termValues[0]);
        unset($termops[0]);



        $i = 1;
        $opType = "";
        $valueArray = [];
        foreach($terms as $term) {

            $termObj = $em->getRepository("AffiliateBundle:Terms")->find($term);

            $campterm = new CampaignTerms();
            $campterm->setCampaignId($campaign);
            $campterm->setCommissionId($commissionObj);
            $campterm->setTermId($termObj);

            if (is_array($termValues[$term]) && sizeof($termValues[$term]) > 1) {
                $opType = "list";
            } else {
                $opType = "op";
            }

            $valueArray['type'] = $opType;
            $valueArray['op_type'] = $termops[$i];
            $valueArray['values']  = $termValues[$term];

            $campterm->setTermValues(json_encode($valueArray));
            $em->persist($campterm);
            $em->flush();

            $i++;
        }

        $tr = $this->get('translator');
        $this->get('session')->getFlashBag()->add('terms_notice', $tr->trans('terms.added', array(), 'AffiliateBundle'));

        return $this->redirect($this->generateUrl("campaign_commission_manager", array('campaign_id' => $campaign->getId())));
    }
}
