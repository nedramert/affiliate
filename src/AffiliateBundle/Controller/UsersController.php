<?php

namespace AffiliateBundle\Controller;

use AffiliateBundle\Entity\Users;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;
use UsersBundle\Entity\UserCampaign;
use UsersBundle\Entity\UserDomains;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class UsersController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/users/{users}", name="user_detail")
     */
    public function detailAction(Users $users, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $tr = $this->get('translator');

        if ($request->query->get('enableDomain')) {
            try {
                $domainId = $request->query->get('enableDomain');
                $domain = $em->getRepository("UsersBundle:UserDomains")->find($domainId);
                $domain->setEnabled(0);
                $em->merge($domain);
                $em->flush();

                $this->get('session')->getFlashBag()->add('user_notice', $tr->trans('user.user_domain_disabled', array(), 'AffiliateBundle'));
                $this->redirectToRoute("user_detail", array('users' => $users));
            } catch(Exception $e) {
                $this->get("affiliate.helper.log")->addLog($e->getMessage());
                $this->get('session')->getFlashBag()->add('user_notice', $e->getMessage());
                $this->redirectToRoute("user_detail", array('users' => $users));
            }
        }

        if ($request->query->get('disableDomain')) {
            try {
                $domainId = $request->query->get('disableDomain');
                $domain = $em->getRepository("UsersBundle:UserDomains")->find($domainId);
                $domain->setEnabled(1);
                $em->merge($domain);
                $em->flush();

                $this->get('session')->getFlashBag()->add('user_notice', $tr->trans('user.user_domain_enabled', array(), 'AffiliateBundle'));
                $this->redirectToRoute("user_detail", array('users' => $users));
            } catch (Exception $e) {
                $this->get("affiliate.helper.log")->addLog($e->getMessage());
                $this->get('session')->getFlashBag()->add('user_notice', $e->getMessage());
                $this->redirectToRoute("user_detail", array('users' => $users));
            }
        }



        $domains = $em->getRepository('UsersBundle:UserDomains')->findBy(array('user' => $users));
        $returnData = [
            'user' => $users,
            'domains' => $domains
        ];

        return $this->render('AffiliateBundle:User:detail.html.twig', $returnData);
    }

    /**
     * @param UserCampaign $userCampaign
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/users/report/{userCampaign}", name="admin_user_camp_report")
     */
    public function reportDetailAction(UserCampaign $userCampaign, Request $request)
    {

        if($request->isXmlHttpRequest()){

            $data = array();
            $data['startDate'] = date("Y-m-d H:i:s", strtotime("first day of this month"));
            $data['endDate'] = date("Y-m-d H:i:s", strtotime("last day of this month"));
            $data['user_id'] = $userCampaign->getUser()->getId();

            $summaryArray = $this->get('reports_service')->getDetail($userCampaign, $data);
            return new JsonResponse($summaryArray);

        }

        return $this->render('AffiliateBundle:User:user_camp_report.html.twig', array(
            'user' => $userCampaign->getUser(),
            'usercamp' => $userCampaign
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/users", name="users")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository("AffiliateBundle:Users")->findAll();

        $returnData = [
            'users' => $users
        ];

        return $this->render('AffiliateBundle:User:list.html.twig', $returnData);
    }

    /**
     * @Route("/users/{user_id}/block", name="user_block")
     * @ParamConverter("user", class="AffiliateBundle:Users", options={"id" = "user_id"})
     */
    public function blockAction(Users $user) {

        $tr = $this->get('translator');
        $em = $this->getDoctrine()->getManager();

        $user->setLocked(true);
        $em->merge($user);
        $em->flush();

        $this->get('session')->getFlashBag()->add('user_notice', $tr->trans('user.ban_done', array(), 'AppBundle'));
        return $this->redirectToRoute("user_detail", array('users' => $user->getId()));
    }

    /**
     * @Route("/users/{user_id}/unblock", name="user_unblock")
     * @ParamConverter("user", class="AffiliateBundle:Users", options={"id" = "user_id"})
     */
    public function unblockAction(Users $user) {

        $tr = $this->get('translator');
        $em = $this->getDoctrine()->getManager();

        $user->setLocked(false);
        $em->merge($user);
        $em->flush();

        $this->get('session')->getFlashBag()->add('user_notice', $tr->trans('user.unban_done', array(), 'AppBundle'));
        return $this->redirectToRoute("user_detail", array('users' => $user->getId()));
    }
}
