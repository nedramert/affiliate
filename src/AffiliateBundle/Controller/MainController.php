<?php

namespace AffiliateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/search", name="admin_search")
     */
    public function searchAction(Request $request) {
        if ($request->isMethod('POST')) {
            $keyword = $request->get('keyword');

            $router = $this->container->get('router');
            $collection = $router->getRouteCollection();
            $allRoutes = $collection->all();


            $routes = "";
            //var_dump($allRoutes); exit;
            foreach($allRoutes as $route => $params) {
                $defaults = $params->getPath();


                if (isset($defaults))
                {
                    $searchKey = str_replace("/", "", $defaults);

                    //if (preg_match("/^".$keyword."$/", $searchKey) && !empty($searchKey)) {
                    if (strpos($defaults ,$keyword) !== false && strpos($defaults, "{") === false) {
                        //echo $keyword. " - ".$searchKey."<br>";
                        if (!isset($routes[$defaults])) {
                            $routes[$defaults] = array();

                            $routes[$defaults][]= $route;
                        }
                    }
                }
            }

            $returnData = [
                'keyword' => $keyword,
                'results' => $routes
            ];

            return $this->render('AffiliateBundle:Search:index.html.twig', $returnData);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/", name="admin_home")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $totalCampaign = $em->getRepository("AffiliateBundle:Campaigns")->totalCampaign();
        $totalUsers = $em->getRepository("AffiliateBundle:Users")->totalUsers();
        $totalContent = $em->getRepository("AffiliateBundle:Content")->totalContent();

        $usersByDay = $em->getRepository("AffiliateBundle:Users")->usersByDay();


        $returnData = [
            'totalCampaign' => $totalCampaign,
            'totalUsers' => $totalUsers,
            'totalContent' => $totalContent,
            'usersByDay' => $usersByDay
        ];
        return $this->render('AffiliateBundle:Dashboard:index.html.twig', $returnData);
    }
}
