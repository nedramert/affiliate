<?php

namespace AffiliateBundle\Controller;

use AffiliateBundle\Entity\Content;
use AffiliateBundle\Form\ContentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ContentController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/contents", name="contents")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();


        $image_contents = $em->getRepository("AffiliateBundle:Content")->findBy(['objectType' => 'image']);
        $text_contents = $em->getRepository("AffiliateBundle:Content")->findBy(['objectType' => 'text']);
        $video_contents = $em->getRepository("AffiliateBundle:Content")->findBy(['objectType' => 'video']);

        $returnData = [
            'image_contents' => $image_contents,
            'text_contents' => $text_contents,
            'video_contents' => $video_contents
        ];

        return $this->render('AffiliateBundle:Content:list.html.twig', $returnData);
    }

    /**
     * @param Content $content
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/contents/{content}/delete", name="content_delete")
     */
    public function deleteAction(Content $content) {
        $em = $this->getDoctrine()->getManager();
        $tr = $this->get('translator');
        if ($content && $content->getObjectType() != 'plain/text') {
            $aws = $this->get("affiliate.helper.aws");

            $deleteObjectArray = [
                'Bucket' => $this->container->getParameter("bucket"),
                'Key'    => $content->getObjectKey()
            ];

            try {
                $aws->deleteObject($deleteObjectArray);
                $em->remove($content);
                $em->flush();

                $this->get('session')->getFlashBag()->add('content_notice', $tr->trans('content.deleted', array(), 'AffiliateBundle'));
                return $this->redirectToRoute("contents");
            } catch(Exception $e) {
                $this->get('session')->getFlashBag()->add('content_notice', $e->getMessage());
                return $this->redirectToRoute("contents");
            }

        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/contents/add", name="content_add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request) {

        $em = $this->getDoctrine()->getEntityManager();
        $tr = $this->get('translator');

        $content = new Content();
        $form = $this->createForm(new ContentType(), $content);


        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $data = $form->getData();
                $contentFile = $data->getObjectKey();
                $contentText = $data->getObjectText();

                if (!empty($contentFile)) {

                    // Dosya Girildiyse
                    $fileSize = $contentFile->getSize();
                    if (empty($fileSize))
                        $fileSize = 0;


                    $collectCDNData = $this->get("affiliate.cdn.manager")->upload($contentFile);

                    // "/" karakterine göre bölümleyip ilk kısmı alıyoruz. Örnek beklenen inputlar: image/png, video/mpeg4 vs..
                    $fileType = explode('/', $contentFile->getmimeType())[0];
                    //Hata olursa exception fırlatıcak. Fakat şimdilik handle etmeyeceğim. Hata dönmekle felan uğraşması zor şimdilik.
                    $content->setObjectType($fileType);

                    $content->setObjectKey($collectCDNData['fileName']);
                    $content->setObjectUrl($collectCDNData['cdn_url']);
                    $content->setObjectSize($fileSize);
                    $content->setObjectText("");
                    $em->persist($content);
                    $em->flush();

                } else if (!empty($contentText)) {
                    // Dosya girilmedi ve metin boş.

                    $content->setObjectType(Content::TYPE_TEXT);
                    $content->setObjectSize(0);
                    $content->setObjectKey("");
                    $content->setObjectUrl("");
                    $content->setObjectText($contentText);
                    $em->persist($content);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('content_notice', $tr->trans('content.addedanduploaded', array(), 'AffiliateBundle'));
                $this->redirectToRoute("contents");
            }
        }

        return $this->render('AffiliateBundle:Content:add.html.twig', array('form' => $form->createView()));
    }
}
