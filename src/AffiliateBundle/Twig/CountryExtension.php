<?php
namespace AffiliateBundle\Twig;

use Symfony\Component\Intl\Intl;
use Doctrine\ORM\EntityManager;

class CountryExtension extends \Twig_Extension
{
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('countryName', array($this, 'countryName')),
        );
    }

    public function countryName($countryCode){

        /*
        $findc = $this->entityManager->getRepository("UsersBundle:Countries")->findBy(array('countryCode' => $countryCode));
        $countryName = "";

        foreach($findc as $country) {
            $countryName =  $country->getCountryName();
        }
        return $countryName;
        */
        return Intl::getRegionBundle()->getCountryName($countryCode);
    }

    public function getName()
    {
        return 'country_extension';
    }
}