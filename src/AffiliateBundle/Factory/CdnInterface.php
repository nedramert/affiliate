<?php
namespace AffiliateBundle\Factory;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface CdnInterface {
    public function upload(UploadedFile $file);
}