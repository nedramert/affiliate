<?php
namespace AffiliateBundle\Factory;


use AffiliateBundle\Helpers\Aws_Helper;
use AffiliateBundle\Helpers\Azure_Helper;

class CdnFactory {

    public static function get($cdn_type, $awsclient, $bucket, $azure_account, $azure_key, $azure_protocol, $azure_default_container) {

        $cdn_service = "";
        switch($cdn_type) {
            case "amazon":
                $cdn_service = new Aws_Helper($awsclient, $bucket);
                break;

            case "azure":
                $cdn_service = new Azure_Helper($azure_account, $azure_key, $azure_protocol, $azure_default_container);
                break;
        }

        return $cdn_service;
    }

}