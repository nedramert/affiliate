<?php
/**
 * Created by PhpStorm.
 * User: mesut
 * Date: 03.03.2016
 * Time: 15:25
 */

namespace UsersBundle\Events;

use AffiliateBundle\Entity\Users;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\Container;
use UsersBundle\Exceptions\UniqueException;

class DoctrineListener
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function preUpdate(LifecycleEventArgs $lifecycleEventArgs)
    {
        $this->em = $lifecycleEventArgs->getEntityManager();
        $entity = $lifecycleEventArgs->getEntity();

        if($entity instanceof Users){
            $this->emailIsUsed($entity);
        }
    }

    public function emailIsUsed(Users $user)
    {
        $result = $this->em->getRepository('AffiliateBundle:Users')->findBy(array('emailCanonical' => $user->getEmailCanonical()));

        if(count($result) != 0){

            $message = $this->container->get('translator')->trans('profile.error_unique_email', [], 'AppBundle');
            throw new UniqueException($message);
        }

    }
}