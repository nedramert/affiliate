<?php

namespace UsersBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations AS MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\HasLifecycleCallbacks;


/**
 * @MongoDB\Document(collection="unique_values")
 * @HasLifecycleCallbacks
 */
class UserCampDocument{

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Integer
     */
    protected $site_id;

    /**
     * @MongoDB\Integer
     */
    protected $camp_id;

    /**
     * @MongoDB\Integer
     */
    protected $content_id;

    /**
     * @MongoDB\String
     */
    protected $landing_url;
    /**
     * @MongoDB\String
     */
    protected $content;

    /**
     * @MongoDB\ReferenceOne(targetDocument="UserDocument", inversedBy="campaigns")
     */
    protected $user;

    /**
     * @MongoDB\String
     * @MongoDB\Index(unique=true)
     */
    protected $unique_id;

    /**
     * @return mixed
     */

    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getLandingUrl()
    {
        return $this->landing_url;
    }

    /**
     * @param $landing_url
     * @return $this
     */
    public function setLandingUrl($landing_url)
    {
        $this->landing_url = $landing_url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * @param $site_id
     * @return $this
     */
    public function setSiteId($site_id)
    {
        $this->site_id = $site_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCampId()
    {
        return $this->camp_id;
    }

    /**
     * @param $camp_id
     * @return $this
     */
    public function setCampId($camp_id)
    {
        $this->camp_id = $camp_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContentId()
    {
        return $this->content_id;
    }

    /**
     * @param $content_id
     * @return $this
     */
    public function setContentId($content_id)
    {
        $this->content_id = $content_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUniqueId()
    {
        return $this->unique_id;
    }

    /**
     * @param $unique_id
     * @return $this
     */
    public function setUniqueId($unique_id)
    {
        $this->unique_id = $unique_id;

        return $this;
    }
}