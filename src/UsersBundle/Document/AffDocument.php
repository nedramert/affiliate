<?php

namespace UsersBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations AS MongoDB;

/**
 * @MongoDB\Document()
 */
class AffDocument{

    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="UserCampDocument", inversedBy="unique_id")
     */
    protected $unique_id;

    /**
     * @MongoDB\Date
     */
    protected $time;

    /**
     * @MongoDB\String
     */
    protected $referrerURL;

    /**
     * @MongoDB\String
     */
    protected $userAgent;

    /**
     * @MongoDB\String
     */
    protected $ipAddress;

    /**
     * @MongoDB\String
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param mixed $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUniqueId()
    {
        return $this->unique_id;
    }

    /**
     * @param mixed $unique_id
     */
    public function setUniqueId($unique_id)
    {
        $this->unique_id = $unique_id;
    }

    /**
     * @return mixed
     */
    public function getReferrerURL()
    {
        return $this->referrerURL;
    }

    /**
     * @param mixed $referrerURL
     */
    public function setReferrerURL($referrerURL)
    {
        $this->referrerURL = $referrerURL;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param mixed $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }
}