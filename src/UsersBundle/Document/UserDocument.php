<?php

namespace UsersBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations AS MongoDB;


/**
 * @MongoDB\Document(collection="users")
 */
class UserDocument{

    /**
     * @MongoDB\Id
     */
    public $id;

    /**
     * @MongoDB\String
     */
    public $email;

    /**
     * @MongoDB\String
     */
    public $username;

    /**
     * @MongoDB\Date
     */
    public $insert_date;

    /**
     * @MongoDB\Int
     */
    public $user_id;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @MongoDB\ReferenceMany(targetDocument="UserCampDocument", mappedBy="user")
     */
    public $campaigns;

    /**
     * @return mixed
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @param $campaigns
     * @return $this
     */
    public function setCampaigns($campaigns)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInsertDate()
    {
        return $this->insert_date;
    }

    /**
     * @param $insert_date
     * @return $this
     */
    public function setInsertDate($insert_date)
    {
        $this->insert_date = $insert_date;

        return $this;
    }
}