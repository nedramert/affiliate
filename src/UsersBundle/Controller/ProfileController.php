<?php

namespace UsersBundle\Controller;

use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UsersBundle\Exceptions\UniqueException;
use UsersBundle\Form\RegistrationType;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\FilterUserResponseEvent;

/**
 * Class ProfileController
 * @package UsersBundle\Controller
 *
 * @Route("/profile")
 */
class ProfileController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction()
    {
        $user = $this->getUser();
        $regType = new RegistrationType($this->get('doctrine.orm.entity_manager'), $user);
        $user_form = $this->createForm($regType, $user);

        return $this->render('UsersBundle:Profile:show.html.twig', array('user_form' => $user_form->createView()));
    }


    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $registrationType = new RegistrationType($this->get('doctrine.orm.entity_manager'), $user);
        $form = $this->createForm($registrationType, $user);

        $form->submit($request->request->get($form->getName()), false);

        if ($form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);

            try{
                $userManager->updateUser($user);
            }catch (UniqueException $ex){
                $this->get('session')->getFlashBag()->add('notice_type', "error");
                $this->get('session')->getFlashBag()->add('notice_title', $this->get('translator')->trans('general.msg_error',[],'AppBundle'));
                $this->get('session')->getFlashBag()->add('user_notice', $ex->getMessage());

                return $this->redirectToRoute('fos_user_profile_show');
            }

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->redirectToRoute('fos_user_profile_show');
    }
}