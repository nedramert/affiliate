<?php

namespace UsersBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends BaseController {

    public function renderLogin(array $data) {
        $requestAttributes = $this->container->get('request')->attributes;

        $login_url = $requestAttributes->get('_route');
        if ($login_url == 'admin_login' || $login_url == 'fos_user_security_login') {
            $template = sprintf('UsersBundle:Security:login.html.twig');
        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }
        return $this->container->get('templating')->renderResponse($template, $data);
    }

    public function loginAction(Request $request)
    {



        if( $this->container->get('security.authorization_checker')->isGranted('ROLE_USER') ){
            //var_dump($this->container->get('security.token_storage')->getToken()->getUser()); exit;
            return new RedirectResponse($this->container->get('router')->generate('user_home', array()));
        }

        if( $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ){

            // IS_AUTHENTICATED_FULLY also implies IS_AUTHENTICATED_REMEMBERED, but IS_AUTHENTICATED_ANONYMOUSLY doesn't

            if($this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')){
                return new RedirectResponse($this->container->get('router')->generate('admin_home', array()));
            }
            //return new RedirectResponse($this->container->get('router')->generate('user_home', array()));

            // of course you don't have to use the router to generate a route if you want to hard code a route
        }

        return parent::loginAction($request);
    }
}
