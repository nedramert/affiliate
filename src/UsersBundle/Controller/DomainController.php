<?php

namespace UsersBundle\Controller;

use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UsersBundle\Entity\UserDomains;
use UsersBundle\Form\UserDomainsType;

/**
 * Class DomainController
 * @package UsersBundle\Controller
 * @Route("/domain")
 */
class DomainController extends Controller
{

    /**
     * @Route("/add", name="user_domain_add")
     */
    public function domainAddAction(Request $request)
    {
        $domain = new UserDomains();
        $form = $this->createForm(new UserDomainsType(), $domain);

        if($request->isMethod('POST')){

            $form->handleRequest($request);
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();
            if($form->isValid()){
                try{

                    $domain->setName($data->getName())
                        ->setUser($this->getUser())
                        ->setEnabled(true);

                    $em->persist($domain);
                    $em->flush();

                }catch (ORMException $ex){
                    die($ex->getMessage());
                }

                return $this->redirectToRoute('user_domain_list');
            }
        }

        return $this->render('UsersBundle:Domains:add.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/list", name="user_domain_list")
     */
    public function domainListAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $domains = $em->getRepository('UsersBundle:UserDomains')->findBy(array('user' => $this->getUser()));

        return $this->render('UsersBundle:Domains:list.html.twig', array("domains" => $domains));
    }

    /**
     * @Route("/detail/{domain}", name="user_domain_detail")
     */
    public function domainDetailAction(UserDomains $domain)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $camps = $em->getRepository('UsersBundle:UserCampaign')->findBy(array('domain' => $domain));

        return $this->render('@Users/Domains/detail.html.twig',array('domain' => $domain,'campaings' => $camps));
    }

}
