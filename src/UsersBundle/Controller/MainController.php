<?php

namespace UsersBundle\Controller;

use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MainController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/", name="user_home")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $firstDayOfMonth = date("Y-m-d",strtotime("first day of this month"));
        $lastDayOfMonth = date("Y-m-d",strtotime("last day of this month"));
        try{

            $myCamps = $em->getRepository('UsersBundle:UserCampaign')->totalCampaigns($user->getId());
            $myActiveCamps = $em->getRepository('UsersBundle:UserCampaign')->totalCampaigns($user->getId(), array('active' => 1));
            $stats = $em->getRepository('UsersBundle:Summary')->totalStatsEachCampaign($user->getId(), array('start_date' => $firstDayOfMonth, 'end_date' => $lastDayOfMonth));

        }catch (ORMException $ex){
            die($ex->getMessage());
        }

        return $this->render('UsersBundle:Dashboard:index.html.twig', array(
            'camps' => $myCamps,
            'activecamps' => $myActiveCamps,
            'general_stats' => $stats
        ));
    }
}
