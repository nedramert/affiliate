<?php

namespace UsersBundle\Controller;

use AffiliateBundle\Entity\Content;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UsersBundle\Entity\UserCampaign;

/**
 * Class ReportsController
 * @package UsersBundle\Controller
 * @Route("/reports")
 */
class ReportsController extends Controller
{
    /**
     * @Route("/", name="reports_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $firstDayOfMonth = date("Y-m-d",strtotime("first day of this month"));
        $lastDayOfMonth = date("Y-m-d",strtotime("last day of this month"));
        try{
            $stats = $em->getRepository('UsersBundle:Summary')->totalStatsEachCampaign($user->getId(), array('start_date' => $firstDayOfMonth, 'end_date' => $lastDayOfMonth));

        }catch (ORMException $ex){
            die($ex->getMessage());
        }

        return $this->render('UsersBundle:Reports:index.html.twig', array(
            'general_stats' => $stats
        ));
    }

    /**
     * @Route("/browse", name="reports_browse_search")
     */
    public function browseAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        try{

            $repo_usercamp = $em->getRepository('UsersBundle:UserCampaign');

            $domains = $em->getRepository('UsersBundle:UserDomains')->findBy(array('user' => $user->getId()));
            $all_camps = $em->getRepository('AffiliateBundle:Campaigns')->getActiveCampaigns();
            $campaigns = $repo_usercamp->findBy(array('user' => $user->getId()));
            $content_types  = Content::getTypeConstants();
            $contents = $repo_usercamp->userCampContents($user->getId());

        }catch (ORMException $ex){
            die($ex->getMessage());
        }

        return $this->render('UsersBundle:Reports:filter.html.twig', array(
            'domains' => $domains,
            'user_camps' => $campaigns,
            'all_camps' => $all_camps,
            'content_types' => $content_types,
            'contents' => $contents
        ));
    }

    /**
     * @param UserCampaign $userCampaign
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/detail/{userCampaign}", name="reports_detail")
     */
    public function detailAction(UserCampaign $userCampaign, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();

        if($request->isXmlHttpRequest()){

            $data = array();
            $data['startDate'] = $request->request->get('startDate');
            $data['endDate'] = $request->request->get('endDate');
            $data['user_id'] = $this->getUser()->getId();

            $summaryArray = $this->get('reports_service')->getDetail($userCampaign, $data);
            return new JsonResponse($summaryArray);

        }


        $summaryRepo = $em->getRepository('UsersBundle:Summary');
        $summaries = $summaryRepo->findBy(array('uniqueId' => $userCampaign->getUniqueId()));

        return $this->render('UsersBundle:Reports:test.html.twig', array(
            'summaries' => $summaries,
            'usercamp' => $userCampaign
        ));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/ajax_browse", name="reports_ajax_browse_search")
     */
    public function ajaxBrowseAction(Request $request)
    {
        $filterArray = array();

        /**
         * LIMIT and OffSet
         */
        $filterArray['length'] = $request->request->get('length');
        $filterArray['start'] = $request->request->get('start');

        if(count($user_camps = $request->request->get('user_campaigns')) > 0 && strlen($user_camps[0]) > 0){
            $filterArray['user_campaigns'] = $user_camps;
        }

        if(count($camps = $request->request->get('campaigns')) > 0 && strlen($camps[0]) > 0){
            $filterArray['campaigns'] = $camps;
        }

        if(count($contents = $request->request->get('contents')) > 0 && strlen($contents[0]) > 0){
            $filterArray['contents'] = $contents;
        }

        if(count($content_type = $request->request->get('content_type')) > 0 && strlen($content_type[0]) > 0){
            $filterArray['content_type'] = $content_type;
        }

        if(count($sites = $request->request->get('sites')) > 0 && strlen($sites[0]) > 0){
            $filterArray['sites'] = $sites;
        }

        if(strlen($date_from = $request->request->get('order_date_from'))){
            $filterArray['date_from'] = new \DateTime($date_from);
        }

        if(strlen($date_to = $request->request->get('order_date_to'))){
            $filterArray['date_to'] = new \DateTime($date_to);
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $returnArray = $em->getRepository('UsersBundle:Summary')->reportFilter($user->getId(), $filterArray);
        $filterArray['countGet'] = true;
        $resultCount = $em->getRepository('UsersBundle:Summary')->reportFilter($user->getId(), $filterArray);

        $clearArray = array(
            'recordsFiltered' => count($resultCount),
            'recordsTotal' => count($resultCount)
        );
        $uc_repo = $em->getRepository('UsersBundle:UserCampaign');
        $router = $this->get('router');
        foreach ($returnArray as $key => $array) {

            // TODO: Allah cezamızı verecek.
            $userCamp = $uc_repo->findBy(array('unique_id' => $array[0]->getUniqueId()))[0];


            $clearArray['data'][$key][] = "<a href=" . $router->generate('reports_detail', array('userCampaign' => $userCamp->getId())) . ">" . $userCamp->getId() . "</a>";
            $clearArray['data'][$key][] = $userCamp->getCampName();
            $clearArray['data'][$key][] = $array[0]->getCampId()->getCampName();
            $clearArray['data'][$key][] = $array[0]->getSiteId()->getName();
            $clearArray['data'][$key][] = $array[0]->getContentId()->getObjectType();

            if($array[0]->getContentId()->getObjectType() == Content::TYPE_TEXT){
                $clearArray['data'][$key][] = $array[0]->getContentId()->getObjectText();
            }else{
                $clearArray['data'][$key][] = "<a target='blank' href=" . $array[0]->getContentId()->getObjectUrl() . "><img width='100' height='100' src=" . $array[0]->getContentId()->getObjectUrl() . "></a>";
            }
            $totalClick = $array['totalClick'];
            $totalView = $array['totalView'];
            $clearArray['data'][$key][] = <<<Div

            <div style='width:50%; display:block; float:left; height:100px; background-color: #007BFF;'>
                $totalClick
            </div>
            <div style='width:50%; display:block; float:left; height:100px; background-color: #61CE2C; text-align:center;'>
                $totalView
            </div>
Div;


        }

        return new JsonResponse($clearArray);

    }
}
