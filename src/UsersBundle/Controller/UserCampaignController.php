<?php

namespace UsersBundle\Controller;

use AffiliateBundle\Entity\Campaigns,
    Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    AffiliateBundle\Entity\CampaignContent;
use AffiliateBundle\Entity\Content;
use Doctrine\ORM\ORMException,
    Doctrine\ODM\MongoDB\MongoDBException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use UsersBundle\Document\UserCampDocument,
    UsersBundle\Document\DailyDocument;
use UsersBundle\Document\UserDocument;
use UsersBundle\Entity\UserCampaign;
use UsersBundle\Form\UserCampaignType;
use UsersBundle\Helpers\UniqueIdGenerator;

/**
 * Class MyCampaignController
 * @package UsersBundle\Controller
 * @Route("/campaign")
 */
class UserCampaignController extends Controller
{
    /**
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route ("/", name="camp_home")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // TODO: Authentication sorunu çözülünce burası düzeltilecek.
        $aff_user_id = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $repo = $em->getRepository('UsersBundle:UserCampaign');
        //$my_camps = $repo->findByUser($aff_user_id);

        return new JsonResponse();
    }

    /**
     * @Route ("/add/{campaign}", name="user_camp_add")
     */
    public function campAddAction(Campaigns $campaign)
    {
        $this->get('session')->set('setCampaignID',$campaign->getId());
        $em = $this->getDoctrine()->getManager();
        $contents_types = $em->getRepository('AffiliateBundle:CampaignContent')->getContentTypesByCampId($campaign->getId());

        return $this->render('UsersBundle:MyCampaigns:user_camp_add.html.twig',array(
            'contents_types' => $contents_types,
            'campaign' => $campaign
        ));
    }

    /**
     * @param $type
     * @param Request $request
     * @return mixed
     * @Route("/content_select/{type}", name="user_camp_add_content_select")
     */
    public function contentAddAction($type, Request $request)
    {
        if(!in_array($type, array_values(Content::getTypeConstants()))){
            return $this->redirectToRoute($request->headers->get('HTTP_REFERER'));
        }

        if($request->isMethod('POST')){

            if($request->get('content')){

                $this->get('session')->set('setContentID', $request->get('content'));
                return $this->redirectToRoute('user_camp_last_stage');

            }else{
                return $this->redirectToRoute($request->headers->get('HTTP_REFERER'));
            }

        }

        $em = $this->getDoctrine()->getManager();
        $camp_id = $this->get('session')->get('setCampaignID');

        $campaign = $em->getRepository("AffiliateBundle:Campaigns")->find($camp_id);
        $contents = $em->getRepository('AffiliateBundle:CampaignContent')->getContensByTypeAndCampId($camp_id, $type);
        if(is_null($contents)){
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('user_campaign.not_exists_content',array('%type%' => $type), 'AppBundle'));
            return $this->redirectToRoute("user_camp_add", array('campaign' => $campaign->getId()));
        }

        $view_file = "";
        if($type == Content::TYPE_TEXT){
            $view_file = "UsersBundle:MyCampaigns:user_camp_text_content.html.twig";
        }elseif($type == Content::TYPE_IMAGE){
            $view_file = "UsersBundle:MyCampaigns:user_camp_image_content.html.twig";
        }elseif($type == Content::TYPE_VIDEO){
            $view_file = "";
        }

        return $this->render($view_file, array(
            'contents' => $contents,
            'campaign' => $campaign,
            'type' => $type
        ));
    }

    /**
     * @Route("/last_stage", name="user_camp_last_stage")
     */
    public function campPreviewAction(Request $request)
    {

        if(!$this->get('session')->has('setCampaignID') || !$this->get('session')->has('setContentID')){
            return $this->redirectToRoute($request->headers->get('HTTP_REFERER'));
        }

        $em = $this->getDoctrine()->getManager();

        $campaignId = $this->get('session')->get('setCampaignID');
        $contentId = $this->get('session')->get('setContentID');

        $aff_user_id = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $domains = $em->getRepository('UsersBundle:UserDomains')->findBy(array('user' => $aff_user_id));

        $camp = $em->getRepository('AffiliateBundle:Campaigns')->find($campaignId);
        $content = $em->getRepository('AffiliateBundle:Content')->find($contentId);

        return $this->render("UsersBundle:MyCampaigns:user_camp_get_code.html.twig",
            array(
                "campaign" => $camp,
                "content" => $content,
                "domains" => $domains
            )
        );
    }

    /**
     * @Route("/create_code", name="user_camp_create_code")
     */
    public function createCodeAction(Request $request)
    {
        if($request->isMethod('POST')){

            $em = $this->getDoctrine()->getManager();
            if($request->get('domain')){

                $domainId = $request->get('domain');
                $campaignId = $this->get('session')->get('setCampaignID');
                $contentId = $this->get('session')->get('setContentID');

                $domain = $em->getRepository('UsersBundle:UserDomains')->find($domainId);
                $camp = $em->getRepository('AffiliateBundle:Campaigns')->find($campaignId);
                $content = $em->getRepository('AffiliateBundle:Content')->find($contentId);
                $aff_user = $this->get('security.token_storage')->getToken()->getUser();
                $camp_name = $request->get('camp_name');
                $api_site = $this->getParameter('api_url');

                try{

                    $unique_id = $this->get('users.helper.uniqueidgenerator')->generate($em->getRepository('UsersBundle:UserCampaign'), 'unique_id');
                    $scriptCode = "<script src=\"$api_site/view/" . $aff_user->getId() . "/$unique_id\"></script>";
                    if($content->getObjectType() == Content::TYPE_TEXT){
                        $HTMLCode = "document.write('<a href=\"$api_site/click/".$aff_user->getId()."/".$unique_id."\">" . $content->getObjectText() . "</a>')";
                    }elseif($content->getObjectType() == Content::TYPE_IMAGE){
                        $HTMLCode = "document.write('<a href=\"$api_site/click/".$aff_user->getId()."/".$unique_id."\"><img src=\"" . $content->getObjectUrl() . "\"></a>')";
                    }

                    $userCampaign = new UserCampaign();
                    $userCampaign->setCampaign($camp)
                                 ->setCampName($camp_name)
                                 ->setContent($content)
                                 ->setDomain($domain)
                                 ->setUser($aff_user)
                                 ->setScriptCode($scriptCode)
                                 ->setHtmlCode($HTMLCode)
                                 ->setUniqueId($unique_id);
                    $em->persist($userCampaign);
                    $em->flush();


                    $dm = $this->get('doctrine_mongodb')->getManager();
                    $mongoUserCamp = new UserCampDocument();
                    $user = $dm->getRepository('UsersBundle:UserDocument')->findBy(array('username' => $aff_user->getUsername()));
                    $user= $user[0];

                    $mongoUserCamp->setContent($userCampaign->getHtmlCode())
                        ->setCampId($userCampaign->getCampaign()->getId())
                        ->setContentId($userCampaign->getContent()->getId())
                        ->setSiteId($userCampaign->getDomain()->getId())
                        ->setLandingUrl($camp->getLandingUrl())
                        ->setUniqueId($unique_id)
                        ->setUser($user);

                    $dm->persist($mongoUserCamp);
                    $dm->flush();

                    // Başka kampanyalar oluşturabilmek için sessiondaki campaign ve content değerlerini siliyoruz.
                    $session = $this->get('session');
                    $session->remove('setCampaignID');
                    $session->remove('setContentID');

                    return $this->redirectToRoute("user_camp_my_camp_detail", array("userCampaign" => $userCampaign->getId()));

                }catch (ORMException $ex){
                    die($ex->getMessage());
                }
                catch(MongoDBException $mEx){
                    die($mEx->getMessage());
                }
            }
        }

        return $this->redirectToRoute($request->headers->get('HTTP_REFERER'));
    }

    /**
     * @param UserCampaign $userCampaign
     * @return Response
     *
     * @Route("/my_camp_detail/{userCampaign}", name="user_camp_my_camp_detail")
     */
    public function myCampDetailAction(UserCampaign $userCampaign, Request $request)
    {
        $userCampType = new UserCampaignType($userCampaign->getContent(), $userCampaign->getCampaign());
        $form = $this->createForm($userCampType, $userCampaign);
        if($request->isMethod('POST')){
            $data = $form->getData();
            // handleRequest kullanılmayacak!
            $form->submit($request->request->get($form->getName()), false);
            if($form->isValid()){
                $em = $this->getDoctrine()->getEntityManager();
                $userCampaign->setCampName($data->getCampName());
                $em->merge($userCampaign);
                $em->flush();
            }
        }

        return $this->render('@Users/MyCampaigns/my_camp_detail.html.twig', array('form' => $form->createView(), 'usercamp' => $userCampaign));
    }

    /**
     * @Route("/testdoc")
     */
    public function testdoc()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $user_id = $this->getUser()->getId();
        $dm->getClassMetadata('UsersBundle\Document\AffDocument')->setCollection("aff_" . $user_id);
        $qb = $dm->getRepository('UsersBundle\Document\AffDocument');
        $find = $qb->findBy(array('time' => array('$lte' => new \DateTime()), 'type' => 'C'));

        foreach ($find as $tip) {

            var_dump($tip->getReferrerURL());
        }




        return new Response();
    }

    /**
     * @Route("/all_list", name="all_camp_list")
     */
    public function campListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $allCamps = $em->getRepository('AffiliateBundle:Campaigns')->getActiveCampaigns();

        return $this->render('UsersBundle:MyCampaigns:camplist.html.twig', array("campaigns" => $allCamps));
    }

    /**
     * @Route("/my_camp_list", name="my_camp_list")
     */
    public function myCampListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $myCamps = $em->getRepository('UsersBundle:UserCampaign')->findBy(array('user' => $user));

        return $this->render('UsersBundle:MyCampaigns:my_camp_list.html.twig', array('usercamps' => $myCamps));
    }

    /**
     * @Route("/detail/{campaign}", name="user_camp_detail")
     */
    public function campDetailAction(Campaigns $campaign)
    {
        $em = $this->getDoctrine()->getManager();

        try {

            $campCommissions = $em->getRepository("AffiliateBundle:CampaignsCommission")->getModelsByCampaignId($campaign->getId());
            $relatedContents = $em->getRepository("AffiliateBundle:CampaignContent")->findBy(array('campaignId' => $campaign->getId()));
            $campaignCommissionsTerms = $em->getRepository("AffiliateBundle:CampaignTerms")->findBy(array('campaignId' => $campaign->getId()));

        } catch (ORMException $e) {
            $this->get("affiliate.helper.log")->addLog($e->getMessage());
            $this->get("session")->getFlashBag()->add('campaign_notice', $e->getMessage());
            return $this->redirectToRoute('all_camp_list');
        }

        $returnArray = array(
            'campaign' => $campaign,
            'commissions' => $campCommissions,
            'contents' => $relatedContents,
            'terms' => $campaignCommissionsTerms
        );

        return $this->render('UsersBundle:MyCampaigns:campdetail.html.twig', $returnArray);
    }

}
