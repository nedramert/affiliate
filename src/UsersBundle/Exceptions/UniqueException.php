<?php
/**
 * Created by PhpStorm.
 * User: mesut
 * Date: 03.03.2016
 * Time: 16:41
 */

namespace UsersBundle\Exceptions;


class UniqueException extends \PDOException
{

    public function __construct($message, $table = "", $column = "")
    {
        $returnMessage = "";
        if($table != ""){
            $returnMessage .= "Table: $table <br>";
        }
        if($column != ""){
            $returnMessage .= "Column: $column<br>";
        }

        $returnMessage .= $message;
        parent::__construct($returnMessage);
    }
}