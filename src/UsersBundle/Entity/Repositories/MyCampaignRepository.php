<?php

namespace UsersBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;
use Doctrine\DBAL\Types\Type;

class MyCampaignRepository extends EntityRepository
{

    /**
     * @param $user_id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByUser($user_id)
    {

        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 50;

        $qb = $this->createQueryBuilder('c');
        $qb->from('UsersBundle:UserCampaign','u')
            ->where('u.user', $user_id);

        return $qb->getQuery()
            ->setResultCacheDriver($predis)
            ->setResultCacheLifetime($expire)
            ->getScalarResult();
    }

    public function totalCampaigns($user_id, $data = array())
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 5;
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.user = :user')
           ->select('count(c.id)');

        if(isset($data['active'])){
            $todayDateTime = new \DateTime();
            $qb->leftJoin('c.campaign', 'camp')
               ->andWhere($qb->expr()->orX(
                   $qb->expr()->gte('camp.endDate', ':end_date'),
                   $qb->expr()->isNull('camp.endDate')
               ))
               ->setParameter(':end_date', $todayDateTime);
        }

        return $qb->setParameter(':user', $user_id)
                  ->getQuery()
                  ->setResultCacheDriver($predis)
                  ->setResultCacheLifetime($expire)
                  ->getSingleScalarResult();
    }

    public function userCampContents($user_id)
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 20;

        $qb = $this->createQueryBuilder('u');
        $qb->where('u.user = :user_id');
        $qb->groupBy('u.content');

        return $qb->setParameter(':user_id', $user_id)
                  ->getQuery()
                  ->setResultCacheDriver($predis)
                  ->setResultCacheLifetime($expire)
                  ->getResult();
    }


}
