<?php

namespace UsersBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;
use Doctrine\ORM\Query\Expr;

class SummaryRepository extends EntityRepository
{

    public function totalStatsEachCampaign($user_id, $data = array())
    {
        $predis = new RedisCache();
        $predis->setRedis(new Client());

        $expire = 2 * 60; //2dk
        $qb = $this->createQueryBuilder('s');
        $qb->addSelect('SUM(s.click) as totalClick, SUM(s.view) as totalView');
        $qb->where($qb->expr()->eq('s.userId', $user_id))
           ->groupBy('s.uniqueId');

        if(isset($data['start_date'])){
            $qb->andWhere($qb->expr()->gte('s.insertDate', ':start_date'));
            $qb->setParameter(':start_date', $data['start_date']);
        }

        if(isset($data['end_date'])){
            $qb->andWhere($qb->expr()->lte('s.insertDate',':end_date'));
            $qb->setParameter(':end_date', $data['end_date']);
        }

        return $qb->getQuery()
                  ->setResultCacheDriver($predis)
                  ->setResultCacheLifetime($expire)
                  ->getResult();
    }

    public function summaryReport($user_id, $startDate, $endDate, $unique_id)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where($qb->expr()->eq('s.userId', ':user_id'));
        $qb->andWhere($qb->expr()->gte('s.insertDate', ':sDate'));
        $qb->andWhere($qb->expr()->lte('s.insertDate', ':eDate'));
        $qb->andWhere($qb->expr()->eq('s.uniqueId', ':unique_id'));

        return $qb->setParameter(':sDate', $startDate)
                  ->setParameter(':eDate', $endDate)
                  ->setParameter(':unique_id', $unique_id)
                  ->setParameter(':user_id', $user_id)
                  ->getQuery()
                  ->getResult();
    }

    /**
     * @param $user_id
     * @param array $data
     * @return array
     */
    public function reportFilter($user_id, $data = array())
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where($qb->expr()->eq('s.userId', $user_id));

        $andConditions = $qb->expr()->andX();

        if(isset($data['content_type'])){
            $qb->innerJoin('AffiliateBundle:Content', 'con','WITH', 'con.id = s.contentId');
            $andConditions->add($qb->expr()->in('con.objectType', $data['content_type']));
        }

        if(isset($data['contents'])){
            $andConditions->add($qb->expr()->in("s.contentId", $data['contents']));
        }

        if(isset($data['user_campaigns'])){
            $qb->innerJoin('UsersBundle:UserCampaign', 'camp','WITH', 'camp.unique_id = s.uniqueId');
            $andConditions->add($qb->expr()->in('camp.id', $data['user_campaigns']));
            $qb->addSelect("camp");
        }

        if(isset($data['campaigns'])){
            $andConditions->add($qb->expr()->in('s.campId', $data['campaigns']));
        }

        if(isset($data['sites'])){
            $andConditions->add($qb->expr()->in("s.siteId", $data['sites']));
        }

        if(isset($data['date_from'])){
            $andConditions->add($qb->expr()->gte("s.insertDate", ":date_from"));
            $qb->setParameter(':date_from', $data['date_from']);
        }

        if(isset($data['date_to'])){
            $andConditions->add($qb->expr()->lte("s.insertDate", ":date_to"));
            $qb->setParameter(':date_to', $data['date_to']);
        }

        if(isset($data['countGet'])){
            $qb->add('select', new Expr\Select('s.id'))
               ->andWhere($andConditions)
               ->groupBy('s.uniqueId');
        }else{
            $qb->add('select', new Expr\Select('SUM(s.click) as totalClick, SUM(s.view) as totalView, s'));
            $qb->andWhere($andConditions)
                ->groupBy('s.uniqueId')
                ->setFirstResult($data['start'])
                ->setMaxResults($data['length']);
        }


        return $qb->getQuery()
                  ->getResult();
    }

}
