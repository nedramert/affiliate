<?php

namespace UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * UserCampaign
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UsersBundle\Entity\Repositories\MyCampaignRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UserCampaign
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="\AffiliateBundle\Entity\Users", inversedBy="camps")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="\AffiliateBundle\Entity\Campaigns", inversedBy="users")
     * @ORM\JoinColumn(name="campaign", referencedColumnName="id")
     */
    private $campaign;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="UserDomains", inversedBy="domains")
     * @ORM\JoinColumn(name="domain", referencedColumnName="id")
     */
    private $domain;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="\AffiliateBundle\Entity\Content", inversedBy="user_campaigns")
     * @ORM\JoinColumn(name="content", referencedColumnName="id")
     */
    private $content;

    /**
     * @var integer
     * @ORM\Column(name="unique_id", type="string", unique=true)
     */
    private $unique_id;

    /**
     * @var string
     * @ORM\Column(name="scriptCode", type="text")
     */
    private $scriptCode;

    /**
     * @var string
     * @ORM\Column(name="htmlCode", type="text")
     */
    private $htmlCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     * @ORM\Column(name="campName", type="string")
     */
    private $campName;

    /**
     * @return string
     */
    public function getCampName()
    {
        return $this->campName;
    }

    /**
     * @param string $campName
     * @return $this
     */
    public function setCampName($campName)
    {
        $this->campName = $campName;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getScriptCode()
    {
        return $this->scriptCode;
    }

    /**
     * @param $scriptCode string
     * @return $this
     */
    public function setScriptCode($scriptCode)
    {
        $this->scriptCode = $scriptCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlCode()
    {
        return $this->htmlCode;
    }

    /**
     * @param string $htmlCode
     * @return $this
     */
    public function setHtmlCode($htmlCode)
    {
        $this->htmlCode = $htmlCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getUniqueId()
    {
        return $this->unique_id;
    }

    /**
     * @param int $unique_id
     */
    public function setUniqueId($unique_id)
    {
        $this->unique_id = $unique_id;
    }

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return UserCampaign
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set campaign
     *
     * @param integer $campaign
     *
     * @return UserCampaign
     */
    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return integer
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set domain
     *
     * @param integer $domain
     *
     * @return UserCampaign
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return integer
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set content
     *
     * @param integer $content
     *
     * @return UserCampaign
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return integer
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UserCampaign
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

