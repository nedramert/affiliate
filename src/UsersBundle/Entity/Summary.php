<?php

namespace UsersBundle\Entity;

use AffiliateBundle\Entity\Campaigns;
use Doctrine\ORM\Mapping as ORM;

/**
 * Summary
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UsersBundle\Entity\Repositories\SummaryRepository")
 */
class Summary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="UsersBundle\Entity\UserDomains")
     * @ORM\JoinColumn(referencedColumnName="id", name="site_id", nullable=false)
     */
    private $siteId;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AffiliateBundle\Entity\Campaigns")
     * @ORM\JoinColumn(referencedColumnName="id", name="camp_id", nullable=false)
     */
    private $campId;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AffiliateBundle\Entity\Content")
     * @ORM\JoinColumn(referencedColumnName="id", name="content_id", nullable=false)
     */
    private $contentId;

    /**
     * @var string
     *
     * @ORM\Column(name="unique_id", type="string", length=255)
     */
    private $uniqueId;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AffiliateBundle\Entity\Users")
     * @ORM\JoinColumn(referencedColumnName="id", name="user_id", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_date", type="datetime")
     */
    private $insertDate;

    /**
     * @var string
     *
     * @ORM\Column(name="click", type="integer", nullable=true)
     */
    private $click;

    /**
     * @var string
     *
     * @ORM\Column(name="view", type="integer", nullable=true)
     */
    private $view;

    /**
     * @return string
     */
    public function getClick()
    {
        return $this->click;
    }

    /**
     * @param $click
     * @return $this
     */
    public function setClick($click)
    {
        $this->click = $click;

        return $this;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set siteId
     *
     * @param integer $siteId
     *
     * @return Summary
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;

        return $this;
    }

    /**
     * Get siteId
     *
     * @return integer
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param Campaigns $campId
     * @return $this
     */
    public function setCampId(Campaigns $campId)
    {
        $this->campId = $campId;

        return $this;
    }

    /**
     * Get campId
     *
     * @return Campaigns
     */
    public function getCampId()
    {
        return $this->campId;
    }

    /**
     * Set contentId
     *
     * @param integer $contentId
     *
     * @return Summary
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;

        return $this;
    }

    /**
     * Get contentId
     *
     * @return integer
     */
    public function getContentId()
    {
        return $this->contentId;
    }

    /**
     * Set uniqueId
     *
     * @param string $uniqueId
     *
     * @return Summary
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * Get uniqueId
     *
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Summary
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set insertDate
     *
     * @param \DateTime $insertDate
     *
     * @return Summary
     */
    public function setInsertDate($insertDate)
    {
        $this->insertDate = $insertDate;

        return $this;
    }

    /**
     * Get insertDate
     *
     * @return \DateTime
     */
    public function getInsertDate()
    {
        return $this->insertDate;
    }
}

