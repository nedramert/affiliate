<?php

namespace UsersBundle\Twig;

class JsonExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_encode', array($this, 'encode')),
            new \Twig_SimpleFilter('json_decode', array($this, 'decode'))
        );
    }

    public function encode($data)
    {
        return json_encode($data);
    }

    public function decode($data)
    {
        return json_decode($data);
    }

    public function getName()
    {
        return 'json_extension';
    }
}