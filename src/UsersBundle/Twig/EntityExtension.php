<?php

namespace UsersBundle\Twig;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class EntityExtension extends \Twig_Extension
{

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('get_entity_row', array($this, 'get'))
        );
    }

    public function get($data, $entitySource, $name, $value)
    {
        $result = array();
        try{
            $repository = $this->entityManager->getRepository($entitySource);
            $result = $repository->findBy(array( $name => $value ));
        }catch (ORMException $ex){
            echo $ex->getMessage();
            die("Repo adını kontrol ediniz.");
        }

        return $result;
    }

    public function getName()
    {
        return 'entity_extension';
    }
}