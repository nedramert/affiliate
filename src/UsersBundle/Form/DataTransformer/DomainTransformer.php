<?php
namespace UsersBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use UsersBundle\Entity\UserDomains;
use AffiliateBundle\Helpers\DB_Helper;
use AffiliateBundle\Entity\Users;

class DomainTransformer implements DataTransformerInterface{

    public $db_helper;
    public $user;

    public function __construct(EntityManager $em, Users $user){

        $this->user = $user;
        $this->db_helper = new DB_Helper($em);

    }

    public function transform($data)
    {
        return '';
    }

    public function reverseTransform($data)
    {
        $arrCollection = new ArrayCollection();

        if (strpos($data, ",") !== false) {
            $expData = explode(',', $data);
            foreach ($expData as $domain) {
                $domain = $this->db_helper->fixDomainName($domain);
                $domainObj = new UserDomains();
                $domainObj->setName($domain);
                $domainObj->setEnabled(true);
                $domainObj->setUser($this->user);
                $arrCollection->add($domainObj);
            }
        } else if (!empty($data)) {
            $data = $this->db_helper->fixDomainName($data);
            $domain = new UserDomains();
            $domain->setName($data);
            $domain->setEnabled(true);
            $domain->setUser($this->user);
            $arrCollection->add($domain);
        }

        return $arrCollection;
    }

}