<?php

namespace UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use AffiliateBundle\Entity\Content;
use Doctrine\ORM\EntityRepository;

class UserCampaignType extends AbstractType
{
    private $contentId;
    private $campaignId;
    public function __construct($contentId = null, $campaignId = null)
    {
        $this->contentId = $contentId;
        $this->campaignId = $campaignId;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('unique_id', 'text', array('disabled' => true))
            ->add('scriptCode', 'textarea', array('translation_domain' => 'AppBundle', 'label' => 'user_campaign.created_code'))
            ->add('htmlCode', 'textarea', array('disabled' => true))
            ->add('createdAt', 'date', array('disabled' => true, 'read_only' => true, 'widget' => 'single_text','format' => 'yyyy-MM-dd HH:mm', 'label' => 'user_campaign.created_date', 'translation_domain' => 'AppBundle'))
            ->add('deletedAt', 'date', array('disabled' => true))
            ->add('campName', 'text', array('label_attr' => array('class' => 'control-label'), 'label' => 'user_campaign.name', 'translation_domain' => 'AppBundle'))
            ->add('user', 'entity', array('class' => 'AffiliateBundle\Entity\Users'))
            ->add('campaign', 'entity', array('class' => 'AffiliateBundle\Entity\Campaigns',
                'query_builder' => function(EntityRepository $repository) {
                    $qb = $repository->createQueryBuilder('c');
                    $qb->where('c.id = :campaign');
                    return $qb->setParameter(':campaign', $this->campaignId);
                }))
            ->add('content', 'entity', array('class' => 'AffiliateBundle\Entity\Content',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')->where('c.id = :content')->setParameter(':content', $this->contentId);
                }))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UsersBundle\Entity\UserCampaign'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'usersbundle_usercampaign';
    }
}
