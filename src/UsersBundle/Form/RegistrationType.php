<?php
namespace UsersBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UsersBundle\Form\DataTransformer\DomainTransformer;
use AffiliateBundle\Entity\Users;

class RegistrationType extends AbstractType
{

    private $entityManager;
    private $user;

    public  function  __construct(EntityManager $entityManager, Users $user) {
        $this->entityManager = $entityManager;
        $this->user = $user;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tc', 'text', array('attr' => array('class' => 'form-control'), 'required' => false, 'label' => 'user_register.tc', 'translation_domain' => 'UsersBundle'))
            ->add('taxNo', 'text', array('attr' => array('class' => 'form-control'), 'required' => false, 'label' => 'user_register.taxno', 'translation_domain' => 'UsersBundle'))
            ->add('taxAdmin', 'text', array('attr' => array('class' => 'form-control'), 'required' => false, 'label' => 'user_register.taxadmin', 'translation_domain' => 'UsersBundle'))
            ->add('fullName', 'text', array('attr' => array('class' => 'form-control'), 'label' => 'user_register.fullname', 'translation_domain' => 'UsersBundle'))
            ->add('phone', 'text', array('attr' => array('class' => 'form-control'), 'label' => 'user_register.phone', 'translation_domain' => 'UsersBundle'))
            ->add('email', 'email', array('attr' => array('class' => 'form-control'), 'label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password', 'attr' => array('class' => 'form-control')),
                'second_options' => array('label' => 'form.password_confirmation', 'attr' => array('class' => 'form-control')),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('country', 'country', array('attr' => array('class' => 'form-control', 'id' => 'countrySelect'), 'label' => 'user_register.country', 'translation_domain' => 'UsersBundle'))
            ->add('city', 'entity', array('attr' => array('class' => 'form-control'), 'class' => 'UsersBundle\Entity\Cities', 'label' => 'user_register.city', 'translation_domain' => 'UsersBundle'))
            ->add('address', 'textarea', array('attr' => array('class' => 'form-control'), 'label' => 'user_register.address', 'translation_domain' => 'UsersBundle'))
            ->add('domains', 'text', array('attr' => array('class' => 'form-control select2', 'id' => 'select2_sample1')))
        ;

        $builder->get('domains')->addModelTransformer(new DomainTransformer($this->entityManager, $this->user));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AffiliateBundle\Entity\Users',
            'intention'  => 'registration',
        ));
    }

    public function getParent()
    {
        return 'fos_user_registration';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getName()
    {
        return 'affiliate_user_registration';
    }
}