<?php
namespace UsersBundle\Helpers;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use UsersBundle\Entity\UserCampaign;

class ReportsService{

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * User tarafından oluşturulan kampanyanın detaylarını getirir.
     * @param UserCampaign $userCampaign
     * @param array $data
     * @return array|ORMException|\Exception
     */
    public function getDetail(UserCampaign $userCampaign, $data = array())
    {
        try{

            $startDate = new \DateTime($data['startDate']);
            $endDate =  new \DateTime($data['endDate']);
            $user_id = $data['user_id'];

            $summaryCollectionArray = $this->em->getRepository('UsersBundle:Summary')->summaryReport($user_id, $startDate, $endDate, $userCampaign->getUniqueId());

            $summaryArray = array();
            foreach ($summaryCollectionArray as $key => $summaryObject) {
                $summaryArray[] = array(
                    'click' => ($summaryObject->getClick()?:0),
                    'view' => $summaryObject->getView(),
                    'date' => $summaryObject->getInsertDate()->format('Y-m-d H:i:s')
                );
            }
        }catch (ORMException $oEx){
            return $oEx;
        }

        return $summaryArray;
    }
}