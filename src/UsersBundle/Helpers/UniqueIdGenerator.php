<?php
namespace UsersBundle\Helpers;

use Doctrine\ORM\EntityRepository;

class UniqueIdGenerator
{

    public function generate(EntityRepository $EntityRepository, $field)
    {
        $counter = 0;
        while ($counter <= 3)
        {
            $counter++;
            $id = mt_rand(100000, 999999);
            $item = $EntityRepository->findBy(array($field => $id));

            if (!$item)
            {
                return $id;
            }
        }

        $className = $EntityRepository->getClassName();
        throw new \Exception( $className . ' entity\'niz çok şişmiş unique_id stratejinizi değiştirebilirsiniz.
                              Oluşturacak unique id kalmamış.
                             ');
    }
}
