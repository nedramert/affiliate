<?php
namespace UsersBundle\Helpers;

use Symfony\Component\DependencyInjection\Container;
use UsersBundle\Entity\UserCampaign;

class Mongo_Helper {

    /**
     * @var null|\MongoClient
     */
    private static $connection = null;

    /**
     * @var Container
     */
    private $container;

    /**
     * @param Container $c
     * @return $this
     */
    public function init(Container $c)
    {
        $this->container = $c;
        if(is_null(self::$connection)){
            try{

                self::$connection = new \MongoClient($this->mongoConString());

            }catch (\MongoException $mEx){
                die($mEx->getMessage());
            }
        }

        return $this;
    }

    /**
     * @return string $server
     */
    public function mongoConString(){

        $settings = [
            'mongo_host' => $this->container->getParameter('mongo_host'),
            'mongo_user' => '',
            'mongo_pass' => '',
            'mongo_port' => $this->container->getParameter('mongo_port'),
            'mongo_db'   => $this->container->getParameter('mongo_db')
        ];

        // Default to localhost if not provided.
        if (!isset($settings['mongo_host'])) {
            $settings['mongo_host'] = 'localhost';
        }

        // Build a connection string.
        $connection_string = '';
        if (isset($settings['mongo_user']) && strlen($settings['mongo_user']) > 0) {
            $connection_string = "${settings['mongo_user']}:${$settings['mongo_pass']}@";
        }
        $connection_string .= $connection_string . $settings['mongo_host'];

        if (isset($settings['mongo_port'])) {
            $connection_string = $connection_string . ':' . $settings['mongo_port'];
        }

        $server = 'mongodb://' . $connection_string;
        return $server;
    }

    public function setAffCollectionOfUser($user_id)
    {
        $mongoDB = self::$connection->selectDB($this->container->getParameter('mongo_db'));

        $userCollection = null;
        try{
            $userCollection = $mongoDB->selectCollection('aff_' . $user_id);
            if($userCollection->count() == 0){
                $userCollection = $mongoDB->createCollection('aff_' . $user_id);
                $userCollection->insert(array('inserted' => new \MongoDate()));
            }
        }catch (\MongoException $mEx){
            die($mEx->getMessage());
        }

        if($userCollection instanceof \MongoCollection){
            return true;
        }

        return false;
    }
}