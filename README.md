Affiliate Project
========================

Created Date:   October 6 - 2015, 10:03
Framework:      Symfony2
FW Version:     2.7.5

Bundles
-------
[AffiliateBundle]
    -   Campaign Controller
    -   Commission Controller
    
[UsersBundle]
    -   _parent: FOSUser
